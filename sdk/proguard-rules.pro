# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-assumenosideeffects class kotlin.jvm.internal.Intrinsics { *; }
-keepclasseswithmembers,allowoptimization public class com.bluairspace.sdk.* extends android.view.View {
    public <methods>;
    public <fields>;
}

-keepclasseswithmembers,allowoptimization class com.bluairspace.sdk.model.** { *; }
-keepclasseswithmembers,allowoptimization public interface com.bluairspace.sdk.model.** { *; }

-keepclasseswithmembers,allowoptimization class com.bluairspace.sdk.helper.** {
    public <methods>;
    public <fields>;
}

-keepclasseswithmembers,allowoptimization class com.bluairspace.sdk.util.publicutil.** {
    public <methods>;
    public <fields>;
}

## Adapt Kotlin metadata.
#-adaptkotlinmetadata

# Adapt information about Kotlin file facades.
-adaptresourcefilecontents **.kotlin_module

# Preserve Kotlin metadata.
-keep class kotlin.Metadata { *; }

# Temporarily disable optimization on Kotlin classes.
-keep,includecode,allowobfuscation,allowshrinking @kotlin.Metadata class ** { *; }

-keep class com.wikitude.** { *; }