package com.bluairspace.sdk.activity.ar

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.camera2.CameraAccessException
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bluairspace.sdk.BuildConfig
import com.bluairspace.sdk.R
import com.bluairspace.sdk.activity.QRCodeActivity
import com.bluairspace.sdk.activity.VideoActivity
import com.bluairspace.sdk.activity.common.AbstractActivitySdk
import com.bluairspace.sdk.model.ARScreenData
import com.bluairspace.sdk.model.ArType
import com.bluairspace.sdk.model.networkCall.CurrentLocation
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.sha256
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import com.bluairspace.sdk.util.sdk.CameraUtils
import com.bluairspace.sdk.util.sdk.LogSdk
import com.bluairspace.sdk.util.sdk.tasks.LocationHelper
import com.bluairspace.sdk.view.ProofingDialog
import com.wikitude.architect.ArchitectJavaScriptInterfaceListener
import com.wikitude.architect.ArchitectStartupConfiguration
import com.wikitude.architect.ArchitectView
import org.json.JSONObject
import java.io.File

internal class ARActivity : AbstractActivitySdk(),
        ArchitectView.ArchitectWorldLoadedListener,
        ArchitectJavaScriptInterfaceListener,
        IARView {

    private val TAG = ARActivity::class.java.simpleName

    private var arPresenter: ARPresenter? = null
    private lateinit var architectView: ArchitectView
    private var arExperience = ""

    private var locationCallBack: LocationHelper.LocationResult? = object : LocationHelper.LocationResult() {
        override fun gotLocation(location: CurrentLocation) {
            Global.LOCATION = location
            setLocation()
        }
    }

    override val context: Context
        get() = this

    override fun onCreate(savedInstanceState: Bundle?) {
        LogSdk.v(TAG, getString(R.string.log_oncreate))
        super.onCreate(savedInstanceState)
        initPresenter()
        var savedArKey: String? = Global.AR_KEY
        if (savedInstanceState != null && savedArKey!!.isEmpty()) {
            savedArKey = savedInstanceState.getString(getString(R.string.param_arkey))
            arPresenter?.isFlashlightActive = savedInstanceState.getBoolean(getString(R.string.param_flashlight), false)
        }
        WebView.setWebContentsDebuggingEnabled(true)
        val intent = intent
        val paramArData = getString(R.string.param_sample_data)
        check(intent.hasExtra(paramArData)) {
            javaClass.simpleName + getString(R.string.log_error_ar_screen_data) + paramArData + "."
        }
        arPresenter?.arType = ArType.values()[intent.getSerializableExtra(getString(R.string.param_extra_artype)) as Int]
        arPresenter?.isProofingMode = intent.getBooleanExtra(getString(R.string.param_proofing), false)
        arPresenter?.isSingleScanEnabled = intent.getBooleanExtra(getString(R.string.param_single_scan), false)
        LogSdk.v(TAG, "arType=" + arPresenter?.arType.toString())

        val arScreenData = intent.getSerializableExtra(paramArData) as ARScreenData
        arExperience = arScreenData.path

        val config = ArchitectStartupConfiguration()
        config.licenseKey = savedArKey
        config.cameraPosition = arScreenData.cameraPosition
        config.cameraResolution = arScreenData.cameraResolution
        config.cameraFocusMode = arScreenData.cameraFocusMode

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) config.isCamera2Enabled = false
        else config.isCamera2Enabled = arScreenData.isCamera2Enabled

        architectView = ArchitectView(this)
        architectView.onCreate(config)
        setContentView(architectView)
        architectView.addArchitectJavaScriptInterfaceListener(this)

        try {
            assets.open(getString(R.string.custimization_file)).bufferedReader().use {
                val request = it.readText()
                architectView.callJavascript(String.format(getString(R.string.js_customization), request))
            }
        } catch (e: Throwable) {
            Log.e(TAG, getString(R.string.log_error_customization))
        }

        val extraString = getIntent().getStringExtra(getString(R.string.param_item_string))
        if (arPresenter?.arType == ArType.INSTANTTRACKING) {
            val isText = MainSettings.isInstantTrackingTextStatus
            architectView.callJavascript(String.format(getString(R.string.js_takeData), extraString, isText.toString()))
        } else {
            val needShowTutorial = "" + MainSettings.showMarkerbasedTutorial
            if (Global.SECRET_APP.contains(Global.API_KEY.sha256())) architectView.callJavascript(getString(R.string.js_checkTutorial, needShowTutorial))
            else architectView.callJavascript(getString(R.string.js_checkTutorial, "false"))
            if (!TextUtils.isEmpty(MainSettings.trialEmail)) architectView.callJavascript(getString(R.string.js_setTrial))
            setLocation()
            updateLocation()
        }
    }

    private fun initPresenter(){
        if (arPresenter != null) {
            if (arPresenter?.view == null) arPresenter?.view = this
        } else arPresenter = ARPresenter(this)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        LogSdk.v(TAG, getString(R.string.log_onpostcreate))
        architectView.onPostCreate()
        try {
            architectView.load(getString(R.string.library_path) + arExperience)
            architectView.registerWorldLoadedListener(this)
        } catch (e: Exception) {
            Toast.makeText(this, getString(R.string.log_error_ar_exp_load), Toast.LENGTH_SHORT).show()
            LogSdk.v(TAG, String.format(getString(R.string.log_error_exp_load), arExperience, e))
        }
    }

    override fun onStart() {
        super.onStart()
        initPresenter()
    }

    override fun onResume() {
        super.onResume()
        LogSdk.v(TAG, getString(R.string.log_onresume))
        architectView.onResume()
        initPresenter()
        arPresenter?.sessionStartTimeMarkerless = System.currentTimeMillis() / 1000
        if (arPresenter!!.isFlashlightActive) {
            try {
                CameraUtils.turnFlashLightOn(this)
            } catch (e: CameraAccessException) {
                Log.e(TAG, getString(R.string.log_error_flashlight))
            }
        }
        if (arPresenter!!.isProofingMode && !arPresenter!!.isQRCodeScanned) openScanActivity()
        if (supportFragmentManager.findFragmentByTag(ProofingDialog::class.java.simpleName) != null) {
            (supportFragmentManager.findFragmentByTag(ProofingDialog::class.java.simpleName) as ProofingDialog)
                    .setOnPositiveButtonClickListener(View.OnClickListener{ openScanActivity() })
        }
        if (!arPresenter!!.isSDKInit) {
            arPresenter?.isSDKInit = true
            architectView.callJavascript(String.format(getString(R.string.js_init), arPresenter?.isSingleScanEnabled))
        }
        if (Global.SECRET_APP.contains(Global.API_KEY.sha256())) {
            architectView.callJavascript(String.format(getString(R.string.js_small_loading_spinner)))
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        Handler().post { architectView.callJavascript(getString(R.string.js_onresume)) }
    }

    override fun onPause() {
        LogSdk.v(TAG, getString(R.string.log_onpause))
        AnalyticsUtil.sendMarkerlessDuration()
        architectView.callJavascript(getString(R.string.js_onpause))
        Thread.sleep(300)
        architectView.onPause()
        arPresenter?.sendSessionDurationAnalytics()
        arPresenter?.sendPlayAnalitycs()
        arPresenter?.onPause()
        if (arPresenter?.arType == ArType.INSTANTTRACKING) finish()
        super.onPause()
    }

    override fun onDestroy() {
        architectView.clearCache()
        architectView.onDestroy()
        LocationHelper.removeArTasks()
        locationCallBack = null
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == VideoActivity.FULL_VIDEO_ACTION) {
            if (data == null) architectView.callJavascript(getString(R.string.js_full_screen_loading_finished))
        } else if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                onBackPressed()
                return
            }
            val result = data.getStringExtra(getString(R.string.param_result))
            arPresenter?.isQRCodeScanned = true
            architectView.callJavascript(String.format(getString(R.string.js_enable_proofing), result))
        } else if (resultCode == Activity.RESULT_CANCELED) onBackPressed()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putString(getString(R.string.param_arkey), Global.AR_KEY)
        savedInstanceState.putBoolean(getString(R.string.param_flashlight_short), arPresenter!!.isFlashlightActive)
        savedInstanceState.putBoolean(getString(R.string.param_qr), arPresenter!!.isQRCodeScanned)
        savedInstanceState.putBoolean(getString(R.string.param_sdk_init), arPresenter!!.isSDKInit)
        savedInstanceState.putBoolean(getString(R.string.param_single_scan), arPresenter!!.isSingleScanEnabled)
        savedInstanceState.putInt(getString(R.string.param_artype), arPresenter!!.arType.value)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        arPresenter?.isSDKInit = savedInstanceState.getBoolean(getString(R.string.param_sdk_init), true)
        arPresenter?.isQRCodeScanned = savedInstanceState.getBoolean(getString(R.string.param_qr), false)
        arPresenter?.isSingleScanEnabled = savedInstanceState.getBoolean(getString(R.string.param_single_scan), false)
        arPresenter?.arType = ArType.values()[savedInstanceState.getInt(getString(R.string.param_artype), 1)]
    }

    override fun openQrCodeScanActivity() {
        openScanActivity()
    }

    override fun performOpenQrCodeScanActivity() {
        ProofingDialog.createLeaveDialog().show(supportFragmentManager, View.OnClickListener{
            architectView.callJavascript(getString(R.string.js_scan_new_qr))
        })
    }

    override fun worldWasLoaded(s: String) {}

    override fun worldLoadFailed(i: Int, s: String, s1: String) {
        val message = String.format(getString(R.string.log_error_next_file_loading), s)
        LogSdk.v(TAG, message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onJSONObjectReceived(jsonObject: JSONObject) {
        arPresenter?.onJSONObjectReceived(jsonObject)
    }

    override fun showToast(value: String) {
        Handler(Looper.getMainLooper()).post { Toast.makeText(this@ARActivity, value, Toast.LENGTH_LONG).show() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    @SuppressLint("NewApi")
    override fun isPermissionGranted(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
            false
        } else true
    }

    override fun callJavaScript(value: String) {
        architectView.callJavascript(value)
    }

    override fun captureScreen(captureScreenCallback: ArchitectView.CaptureScreenCallback) {
        if (!isPermissionGranted()) return
        architectView.captureScreen(ArchitectView.CaptureScreenCallback.CAPTURE_MODE_CAM_AND_WEBVIEW, captureScreenCallback)
    }

    private fun openScanActivity() {
        val filename = context.getExternalFilesDir(null)!!.toString() + File.separator
        Thread(Runnable { MainSettings.clearCache(File(filename)) }).start()
        val intent = Intent(this, QRCodeActivity::class.java)
        startActivityForResult(intent, 1)
    }

    private fun setLocation() {
        architectView.callJavascript(getString(R.string.js_set_location, Global.LOCATION.latitude, Global.LOCATION.longitude))
    }

    private fun updateLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return
        LocationHelper.getLocation(this, (3 * 1000).toLong(), locationCallBack!!)
        LocationHelper.getLocation(this, (20 * 1000).toLong(), locationCallBack!!)
    }
}

