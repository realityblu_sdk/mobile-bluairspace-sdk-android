package com.bluairspace.sdk.activity

import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.webkit.*
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity

import com.bluairspace.sdk.R

internal class WebViewActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view_fragment)

        webView = findViewById(R.id.web_view)
        findViewById<View>(R.id.back_button).setOnClickListener { finish() }
        val progress = findViewById<ProgressBar>(R.id.progress_bar)
        if (progress != null) progressBar = progress

        webView.webViewClient = WebClient()
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                if (newProgress > 85) progressBar.visibility = View.GONE
                else setProgressAnimate(progressBar, newProgress)
                super.onProgressChanged(view, newProgress)
            }
        }

        webView.settings.javaScriptEnabled = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.settings.domStorageEnabled = true
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW

        var url = intent.getStringExtra(EXTRA_URL)
        if (url!!.toLowerCase().contains(".pdf")) url = "https://docs.google.com/gview?embedded=true&url=$url"
        if (!url.contains("http")) url = "https://$url"

        webView.loadUrl(url)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) webView.goBack()
        else super.onBackPressed()
    }

    private fun setProgressAnimate(pb: ProgressBar?, progressTo: Int) {
        val animation = ObjectAnimator.ofInt(pb, "progress", pb!!.progress, progressTo)
        animation.duration = 100
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webView.restoreState(savedInstanceState)
    }

    private inner class WebClient : WebViewClient() {

        private var started = false
        private var newUrl = ""

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            started = true
            progressBar.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView, url: String) {
            if (!started) view.loadUrl(url)
            else started = false
            if (newUrl == url) view.clearHistory()
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            proceedUrl(view, Uri.parse(url))
            return true
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            proceedUrl(view, request.url)
            return true
        }

        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
            if (errorCode == -11) {
                newUrl = failingUrl.replace("https", "http", true)
                view.loadUrl(newUrl)
            }
            super.onReceivedError(view, errorCode, description, failingUrl)
        }

        override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (error.errorCode == -11) {
                    newUrl = request.url.toString().replace("https", "http", true)
                    view.loadUrl(newUrl)
                }
            }
            super.onReceivedError(view, request, error)
        }

        private fun proceedUrl(view: WebView, uri: Uri) {
            if (uri.toString().startsWith("mailto:")) startActivity(Intent(Intent.ACTION_SENDTO, uri))
            else if (uri.toString().startsWith("tel:")) startActivity(Intent(Intent.ACTION_DIAL, uri))
            else view.loadUrl(uri.toString())
        }
    }

    companion object {
        private const val EXTRA_URL = "EXTRA_URL"

        fun newInstance(context: Context, url: String) {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra(EXTRA_URL, url)
            context.startActivity(intent)
        }
    }
}
