package com.bluairspace.sdk.activity.ar

import android.content.Context
import com.wikitude.architect.ArchitectView

internal interface IARView {

    fun isPermissionGranted(): Boolean
    fun showToast(value: String)
    fun onBackPressed()
    fun callJavaScript(value: String)
    fun openQrCodeScanActivity()
    fun performOpenQrCodeScanActivity()
    fun captureScreen(captureScreenCallback: ArchitectView.CaptureScreenCallback)
    fun getString(resId: Int): String
    fun getString(resId: Int, vararg formatArgs: Any): String
    val context:Context
}