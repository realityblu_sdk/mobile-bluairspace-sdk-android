package com.bluairspace.sdk.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bluairspace.sdk.R
import com.yanzhenjie.zbar.camera.ScanCallback
import kotlinx.android.synthetic.main.qrcode_activity.*
import org.json.JSONException
import org.json.JSONObject


internal class QRCodeActivity : AppCompatActivity() {

    private val TAG = QRCodeActivity::class.java.simpleName

    private val resultCallback = ScanCallback { result ->
        camera.stop()
        checkForResult(result)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN)
        setContentView(R.layout.qrcode_activity)
        camera.setScanCallback(resultCallback)
        back_button.setOnClickListener { onBackPressed() }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun checkForResult(result: String) {
        try {
            JSONObject(result)
            createReturnIntent(result)
        } catch (e: JSONException) {
            e.printStackTrace()
            Log.e(TAG, "decodeQRCodeResult: Bad format of QRCode")
            camera.start()
        }
    }

    private fun createReturnIntent(result: String?) {
        val intent = Intent()
        if (result != null) {
            intent.putExtra("result", result)
            setResult(Activity.RESULT_OK, intent)
        } else setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onBackPressed() {
        createReturnIntent(null)
    }

    override fun onResume() {
        super.onResume()
        camera.start()
    }

    override fun onPause() {
        camera.stop()
        super.onPause()
    }
}
