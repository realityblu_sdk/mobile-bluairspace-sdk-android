package com.bluairspace.sdk.activity.ar

import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import com.bluairspace.sdk.R
import com.bluairspace.sdk.activity.VideoActivity
import com.bluairspace.sdk.activity.WebViewActivity
import com.bluairspace.sdk.helper.BluUserHelper
import com.bluairspace.sdk.model.ArType
import com.bluairspace.sdk.model.CalendarEvent
import com.bluairspace.sdk.model.ContactEvent
import com.bluairspace.sdk.model.callback.ContentDownloadCallback
import com.bluairspace.sdk.model.callback.PersonalVideoCallback
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.model.exception.NotValidDataException
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.emailPatternError
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.parseCalendarEventError
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.phonePatternError
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.UrlHelper
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import com.bluairspace.sdk.util.sdk.CameraUtils
import com.bluairspace.sdk.util.sdk.IntentUtil
import com.bluairspace.sdk.util.sdk.LogSdk
import com.bluairspace.sdk.util.sdk.tasks.ContentDownloader
import com.bluairspace.sdk.util.sdk.tasks.PersonalVideoDownloadHelper
import com.wikitude.architect.ArchitectView
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.UnsupportedEncodingException
import java.lang.ref.WeakReference
import java.net.URLEncoder
import java.util.*

internal class ARPresenter(view: IARView) {

    private val TAG = ARActivity::class.java.simpleName
    private val viewRef = WeakReference(view)
    internal var view = viewRef.get()

    internal lateinit var arType: ArType

    internal var isProofingMode = false
    internal var isSingleScanEnabled = false
    internal var isQRCodeScanned = false
    internal var isSDKInit = false
    internal var isFlashlightActive: Boolean = false

    internal var playModels = HashMap<Int, Long>()

    private var startDate: Long? = null
    private var endDate: Long? = null
    private var expId = -1
    internal var sessionStartTimeMarkerless: Long = System.currentTimeMillis() / 1000

    private val contentDownloadCallback = object: ContentDownloadCallback {
        override fun sendNextFileLoaded(item: JSONObject) {
            try {
                val url = URLEncoder.encode(item.toString(), getString(R.string.encoder))
                Log.d(TAG, String.format(getString(R.string.log_file_loading), url))
                view.callJavaScript(String.format(getString(R.string.js_file_loading), url))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        }

        override fun sendEndDownloadingModelsJsonNew() {
            LogSdk.v(TAG, getString(R.string.log_file_loading_complete))
            view.callJavaScript(getString(R.string.js_file_loading_complete))
        }
    }

    private val personalVideoCallback = object: PersonalVideoCallback {
        override fun sendNextVideoLoaded(id: Any, videoUrl: String) {
            LogSdk.v(TAG, String.format(getString(R.string.log_file_loading), videoUrl))
            view.callJavaScript(String.format(getString(R.string.js_video_loading), id, videoUrl))
        }
    }

    fun onJSONObjectReceived(jsonObject: JSONObject) {
        try {
            if (jsonObject.has(getString(R.string.param_name)) && jsonObject.getString(getString(R.string.param_name)).equals(getString(R.string.ar_message), ignoreCase = true)) {
                val message = jsonObject.getString(getString(R.string.param_description))
                LogSdk.v(TAG, String.format(getString(R.string.log_object_received), message))
                return
            }
            when (jsonObject.getString(getString(R.string.param_action))) {
                "ClientData" -> prepareClientData()
                "scan_qr_code" -> view?.openQrCodeScanActivity()
                "perform_scan_qr_code" -> view?.performOpenQrCodeScanActivity()
                "capture_screen" -> view?.captureScreen(ArchitectView.CaptureScreenCallback { saveScreenCapture(it) })
                "SceneDownload" -> ContentDownloader.startDownload(jsonObject, contentDownloadCallback)
                "phoneCall" -> preparePhoneCall(jsonObject)
                "PersonalVideo" -> PersonalVideoDownloadHelper.startDownload(jsonObject, personalVideoCallback)
                "changeTextStatus" -> MainSettings.isInstantTrackingTextStatus = false
                "btn_action" -> handleButtonActions(jsonObject)
                "end_trial" -> BluUserHelper.setTrialEmail("", TaskCallback.EMPTY)
                "markerRecognized" -> expId = jsonObject.getInt(getString(R.string.param_exp_id))
                "markerlessDuration" -> AnalyticsUtil.addModel(jsonObject.getInt(getString(R.string.param_param)))
                "showAllMarkerlessModels" -> AnalyticsUtil.setMarkerlessDuration()
                "hideAllMarkerlessModels" -> AnalyticsUtil.pauseMarkerlessDuration()
                "resetMarkerlessModels" -> AnalyticsUtil.sendMarkerlessDuration()
                "experiencePlayStop" -> experiencePlayStop()
                "modelPlayStart" -> modelPlayStart(jsonObject.getInt(getString(R.string.param_param)))
                "showTutorial" -> MainSettings.showMarkerbasedTutorial = jsonObject.getBoolean(getString(R.string.param_param))
                "fullScreen" -> VideoActivity.newInstance(view!!.context, jsonObject.getString(getString(R.string.param_param)))
                "customization" -> checkCustomizationFile(jsonObject)
                "experiencePlayStart" -> {
                    expId = jsonObject.getInt(getString(R.string.param_param))
                    experiencePlayStart(expId)
                }
                "back_to_gallery" -> {
                    AnalyticsUtil.sendMarkerlessDuration()
                    Handler(Looper.getMainLooper()).post { view?.onBackPressed() }
                }
                "flashlightNative" -> {
                    CameraUtils.turnFlashLight(view!!.context, !isFlashlightActive)
                    isFlashlightActive = !isFlashlightActive
                }
                "experienceAction" -> {
                    val actionEvent = jsonObject.getJSONObject(getString(R.string.param_param))
                    AnalyticsUtil.faSendMarkerbasedExpAction(
                            actionEvent.getInt(getString(R.string.param_exp_id)),
                            actionEvent.getInt(getString(R.string.param_type)),
                            actionEvent.getString(getString(R.string.param_id)),
                            actionEvent.getString(getString(R.string.param_name)),
                            actionEvent.getString(getString(R.string.param_value)))
                }
                else -> {
                }
            }
        }catch (e: java.lang.Exception){
            e.printStackTrace()
        }
    }

    private fun getLocalGroupId(): String {
        return when (Global.APPLICATION_ID) {
            "com.ar.realityblu" -> getString(R.string.rbl_code)
            else -> "Other"
        }
    }

    private fun saveScreenCapture(screenCapture: Bitmap?) {
        if (screenCapture == null) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val location = Environment.DIRECTORY_PICTURES + File.separator + getString(R.string.screenshots_path)
            val fileName = getString(R.string.screenshots_capture) + System.currentTimeMillis()
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                put(MediaStore.MediaColumns.MIME_TYPE, getString(R.string.image_type))
                put(MediaStore.MediaColumns.RELATIVE_PATH, location)
            }
            val contentResolver = view?.context?.contentResolver
            try {
                val contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                val uri = contentResolver?.insert(contentUri, contentValues)
                val stream = contentResolver?.openOutputStream(uri!!)
                screenCapture.compress(Bitmap.CompressFormat.JPEG, 90, stream)
                view?.showToast("Screen capture saved. Path: $location/$fileName.jpg")
            } catch (e: Throwable) {
                view?.showToast(e.message.toString())
            }
        } else {
            val screenCaptureFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
                    + "/" + File.separator, getString(R.string.screenshots_capture) + System.currentTimeMillis() + ".jpg")
            try {
                val out = FileOutputStream(screenCaptureFile)
                screenCapture.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                view?.showToast(screenCaptureFile.absolutePath)
                val fileUri = Uri.fromFile(screenCaptureFile)
                val mediaScannerIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, fileUri)
                view?.context?.sendBroadcast(mediaScannerIntent)
            } catch (ex: Exception) {
                view?.showToast(ex.message.toString())
            }
        }
    }

    private fun handleButtonActions(jsonObject: JSONObject) {
        val actionMode = jsonObject.getInt(getString(R.string.param_mode))
        val actionValue = jsonObject.getString(getString(R.string.param_param))
        when (actionMode) {
            0 -> WebViewActivity.newInstance(view!!.context, actionValue)
            1,2 -> VideoActivity.newInstance(view!!.context, actionValue)
            4 -> makePhoneCall(actionValue)
            5 -> makeEmail(actionValue)
            6 -> addEventToCalendar(actionValue)
            8 -> addContact(actionValue)
            else -> {
            }
        }
    }

    private fun checkCustomizationFile(jsonObject: JSONObject) {
        val element = jsonObject.getString(getString(R.string.param_element))
        val path = jsonObject.getString(getString(R.string.param_path))
        val androidPath = getString(R.string.cust_path) + path
        try {
            val ins = view!!.context.assets.open(androidPath)
            view?.callJavaScript(String.format(getString(R.string.js_cust_file), element, path))
            ins.close()
        } catch (e: Throwable) {
            Log.e(TAG, String.format(getString(R.string.log_error_cust_file), androidPath))
        }
    }

    private fun prepareClientData() {
        val clientToken = MainSettings.cloudRecognitionClientToken
        val wikitudeCollection = MainSettings.cloudRecognitionARCollection
        val proofingWikitudeCollection = MainSettings.cloudRecognitionProofingARCollection
        var groupId = MainSettings.cloudRecognitionGroupId
        if (TextUtils.isEmpty(groupId)) groupId = getLocalGroupId()
        val value = String.format("%s,%s,%s,%s", clientToken, if (isProofingMode) proofingWikitudeCollection else wikitudeCollection, groupId, UrlHelper.endpoint.markerlessMarkerbasedUrl)
        view?.callJavaScript(String.format(getString(R.string.js_setup_data), value))
    }

    private fun preparePhoneCall(jsonObject: JSONObject) {
        var telString = ""
        if (!jsonObject.isNull(getString(R.string.param_telstring))) {
            try {
                telString = jsonObject.getString(getString(R.string.param_telstring))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        makePhoneCall(telString)
    }

    private fun makePhoneCall(phone: String) {
        try {
            IntentUtil.phoneCallManual(view!!.context, phone)
        } catch (ex: NotValidDataException) {
            ex.printStackTrace()
            view?.showToast(phonePatternError)
        }
    }

    private fun makeEmail(mail: String) {
        try {
            IntentUtil.emailManual(view!!.context, mail)
        } catch (ex: NotValidDataException) {
            ex.printStackTrace()
            view?.showToast(emailPatternError)
        }
    }

    private fun addEventToCalendar(value: String) {
        try {
            val event = CalendarEvent.parse(value)
            IntentUtil.addEventToCalendar(view!!.context, event)
        } catch (ex: Exception) {
            ex.printStackTrace()
            view?.showToast(parseCalendarEventError)
        }
    }

    private fun addContact(value: String) {
        try {
            val event = ContactEvent.parse(value)
            IntentUtil.addContact(view!!.context, event)
        } catch (ex: Exception) {
            ex.printStackTrace()
            view?.showToast(parseCalendarEventError)
        }
    }

    private fun modelPlayStart(modelId: Int) {
        if (!playModels.containsKey(modelId)) playModels[modelId] = System.currentTimeMillis() / 1000
    }

    internal fun sendPlayAnalitycs() {
        if (arType != ArType.INSTANTTRACKING) experiencePlayStop()
    }

    internal fun sendSessionDurationAnalytics() {
        val now = System.currentTimeMillis() / 1000
        val duration = now - sessionStartTimeMarkerless
        AnalyticsUtil.faSendSessionDuration(arType.value, duration.toInt())
        sessionStartTimeMarkerless = now
    }

    private fun experiencePlayStart(expId: Int) {
        if (startDate != null) return
        this.expId = expId
        startDate = System.currentTimeMillis() / 1000
    }

    private fun experiencePlayStop() {
        if (startDate == null) return
        if (endDate != null) return
        endDate = System.currentTimeMillis() / 1000
        val duration = endDate!! - startDate!!
        val expId = this.expId
        AnalyticsUtil.faSendMarkerbasedExpDuration(expId, duration.toInt())
        clearDates()
    }

    private fun clearDates() {
        startDate = null
        endDate = null
        expId = -1
    }

    internal fun onPause(){
        view = null
        ContentDownloader.cancelCurrentJob()
        PersonalVideoDownloadHelper.cancelCurrentJob()
    }

    private fun getString(resId: Int) = view!!.getString(resId)
    private fun getString(resId: Int, vararg formatArgs: Any) = view!!.getString(resId, formatArgs)
}