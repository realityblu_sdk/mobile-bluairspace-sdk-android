package com.bluairspace.sdk.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import com.bluairspace.sdk.BuildConfig
import com.bluairspace.sdk.R
import com.bluairspace.sdk.activity.ar.ARActivity
import com.bluairspace.sdk.activity.common.AbstractActivitySdk
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.videoError
import com.bluairspace.sdk.view.LoadingDialog
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.common.BuildConfig.VERSION_NAME
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.video_activity.*

internal class VideoActivity : AbstractActivitySdk(), Player.EventListener {

    private var url: String = ""

    private var currentPosition = 0L
    private var currentWindowIndex = 0
    private var playWhenReady = true

    private var player: SimpleExoPlayer? = null

    private lateinit var dialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_activity)
        back_button.setOnClickListener { onBackPressed() }
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            ExoPlayer.STATE_READY -> dialog.dismiss()
            else -> super.onPlayerStateChanged(playWhenReady, playbackState)
        }
    }

    override fun onPlayerError(error: ExoPlaybackException) {
        dialog.dismiss()
        Toast.makeText(this, videoError, Toast.LENGTH_SHORT).show()
        super.onPlayerError(error)
    }

    private fun initialize(){
        initView()
        player = SimpleExoPlayer.Builder(this).build()
        videoView.player = player
        player?.addListener(this)
        player?.playWhenReady = playWhenReady
        player?.seekTo(currentWindowIndex, currentPosition)
        player?.prepare(buildMediaSource(Uri.parse(url)), false, false)
    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        val dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "blu-sdk"))
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(uri)
    }

    private fun release(){
        currentPosition = player!!.currentPosition
        currentWindowIndex = player!!.currentWindowIndex
        playWhenReady = player!!.isPlaying
        player?.stop()
        player?.release()
        player = null
    }

    private fun initView() {
        val stackDialog = supportFragmentManager.findFragmentByTag(LoadingDialog.TAG)
        if (stackDialog != null) dialog = stackDialog as LoadingDialog
        else {
            dialog = LoadingDialog()
            supportFragmentManager.beginTransaction().apply {
                add(dialog, LoadingDialog.TAG)
                commitAllowingStateLoss()
            }
        }

        url = intent.getStringExtra(EXTRA_URL)!!
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) initialize()
    }

    override fun onResume() {
        super.onResume()
        if (Util.SDK_INT < 24 || player == null) initialize()
    }

    override fun onPause() {
        if (Util.SDK_INT < 24) release()
        super.onPause()
    }

    override fun onStop() {
        if (Util.SDK_INT >= 24) release()
        super.onStop()
    }

    override fun onBackPressed() {
        setResult(FULL_VIDEO_ACTION)
        finish()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(CURRENT_DURATION, currentPosition)
        outState.putBoolean(PLAY_WHEN_READY, playWhenReady)
        outState.putString(EXTRA_URL, url)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentPosition = savedInstanceState.getLong(CURRENT_DURATION, 0)
        playWhenReady = savedInstanceState.getBoolean(PLAY_WHEN_READY, true)
        url = savedInstanceState.getString(EXTRA_URL)!!
    }

    companion object {

        private const val EXTRA_URL = "EXTRA_URL"
        const val FULL_VIDEO_ACTION = 101
        const val CURRENT_DURATION = "current_duration"
        const val PLAY_WHEN_READY = "playWhenReady"

        fun newInstance(context: Context, url: String) {
            val intent = Intent(context, VideoActivity::class.java)
            intent.putExtra(EXTRA_URL, url)
            (context as ARActivity).startActivityForResult(intent, FULL_VIDEO_ACTION)
        }
    }
}
