package com.bluairspace.sdk.networkAsyncTaskResult

import com.bluairspace.sdk.model.MarkerbasedMarker
import com.bluairspace.sdk.model.MarkerlessExperienceGroup
import com.bluairspace.sdk.model.MarkerlessGroup
import com.bluairspace.sdk.util.publicutil.Global
import retrofit2.http.GET
import retrofit2.http.Path

internal interface ApiMarkerlessMarkerBased {

    @GET("getListMarkerlessExperience/" + Global.PLATFORM + "/{app_id}")
    suspend fun getSceneList(@Path("app_id") appId: String): List<MarkerlessGroup>

    @GET("getGroupsMarkerlessExperience/" + Global.PLATFORM + "/{app_id}/{list_id}")
    suspend fun getGroupList(@Path("app_id") appId: String, @Path("list_id") sceneId: Int): List<MarkerlessExperienceGroup>

    @GET("getMarkerbasedTargets/" + Global.PLATFORM + "/{app_id}")
    suspend fun getMarkerbasedTargets(@Path("app_id") appId: String): List<MarkerbasedMarker>

    @GET("getMarkerlessExperienceById/" + Global.PLATFORM + "/{app_id}/{id}")
    suspend fun getMarkerlessExperienceById(@Path("app_id") appId: String?, @Path("id") expId: Int): List<MarkerlessExperienceGroup>

}
