package com.bluairspace.sdk.network.endpoints

class EndpointProd : Endpoint {

    private val commonUrl: String
        get() = COMMON_URL

    override val name: String
        get() = NAME

    override val markerlessMarkerbasedUrl: String
        get() = EXPERIENCES

    override val initUrl: String
        get() = INIT_URL

    override val contactUrl: String
        get() = CONTACTS_URL

    override val persVideoUrlUserData: String
        get() = PERSONAL_VIDEO_URL

    override val persVideoUrlGenerateVideo: String
        get() = PERSONAL_VIDEO_GENERATE_URL

    override val bluAirSpaceUrl: String
        get() = BLUAIRSPACE_URL

    private companion object {
        private const val COMMON_URL = "https://api-internal.realityblu.com/prod"
        private const val NAME = "prod"
        private const val EXPERIENCES = "$COMMON_URL-mobile-experiences/"
        private const val INIT_URL = "$COMMON_URL-mobile-init/initMobileApp/"
        private const val CONTACTS_URL = "$COMMON_URL-mobile-support/"
        private const val PERSONAL_VIDEO_URL = "$COMMON_URL-pv/createPV/"
        private const val PERSONAL_VIDEO_GENERATE_URL = "$COMMON_URL-mobile-experiences/getPersonVideoForMobile/"
        private const val BLUAIRSPACE_URL = "https://bluairspace.realityblu.com"
    }
}
