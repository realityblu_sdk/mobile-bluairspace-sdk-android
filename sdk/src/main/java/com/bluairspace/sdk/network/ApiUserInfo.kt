package com.bluairspace.sdk.network

import com.bluairspace.sdk.model.networkCall.CommonResponse
import com.bluairspace.sdk.model.networkCall.UserInfo
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

internal interface ApiUserInfo {

    @POST
    suspend fun createPv(@Url url: String, @Body data: UserInfo): CommonResponse

    @POST
    suspend fun getPersonVideoForMobile(@Url url: String, @Body data: UserInfo.UserInfoRequest): ResponseBody
}
