package com.bluairspace.sdk.network

import com.bluairspace.sdk.model.networkCall.InitApp
import com.bluairspace.sdk.util.publicutil.Global
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Path

internal interface ApiInitApp {

    @GET(Global.PLATFORM + "/" + Global.VERSION_SDK_NAME + "/{app_id}")
    suspend fun getInit(
            @HeaderMap headers: Map<String, String>,
            @Path("app_id") appId: String): InitApp.ResponseInitApp?
}
