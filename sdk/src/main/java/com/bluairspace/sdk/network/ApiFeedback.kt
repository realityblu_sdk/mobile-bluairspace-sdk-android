package com.bluairspace.sdk.network

import com.bluairspace.sdk.model.networkCall.CommonResponse
import com.bluairspace.sdk.model.networkCall.FeedbackData

import retrofit2.http.Body
import retrofit2.http.POST

internal interface ApiFeedback {

    @POST("sendFeedback")
    suspend fun sendFeedback(@Body requestContactUs: FeedbackData.FeedbackRequest): CommonResponse
}
