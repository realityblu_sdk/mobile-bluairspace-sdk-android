package com.bluairspace.sdk.network

import com.bluairspace.sdk.networkAsyncTaskResult.ApiMarkerlessMarkerBased
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.UrlHelper.endpoint
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

internal object ApiFactory {

    private lateinit var httpClient: OkHttpClient

    fun getInitRetrofit(): ApiInitApp = getRetrofit(endpoint.initUrl).create(ApiInitApp::class.java)
    fun getDataRetrofit(): ApiMarkerlessMarkerBased = getRetrofit(endpoint.markerlessMarkerbasedUrl).create(ApiMarkerlessMarkerBased::class.java)
    fun getSavePersonalInfoRetrofit(): ApiUserInfo = getRetrofit("https://your.api.url/").create(ApiUserInfo::class.java)
    fun getLoadPersonalInfoRetrofit(): ApiUserInfo = getRetrofit(endpoint.persVideoUrlGenerateVideo).create(ApiUserInfo::class.java)
    fun getFeedbackRetrofit(): ApiFeedback = getRetrofit(endpoint.contactUrl).create(ApiFeedback::class.java)

    private fun getRetrofit(url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .client(getClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getClient(): OkHttpClient {
        return if (!::httpClient.isInitialized) {
            httpClient = buildClient()
            httpClient
        } else httpClient
    }

    private fun buildClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .apply {
                    connectTimeout(15, TimeUnit.SECONDS)
                    readTimeout(15, TimeUnit.SECONDS)
                    if (Global.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.level = HttpLoggingInterceptor.Level.BODY
                        addInterceptor(logging)
                    }
                }.build()
    }
}
