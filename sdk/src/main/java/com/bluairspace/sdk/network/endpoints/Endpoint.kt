package com.bluairspace.sdk.network.endpoints

interface Endpoint {

    val name: String

    val markerlessMarkerbasedUrl: String
    val initUrl: String
    val contactUrl: String


    val persVideoUrlUserData: String
    val persVideoUrlGenerateVideo: String

    val bluAirSpaceUrl: String

}
