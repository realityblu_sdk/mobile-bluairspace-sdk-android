package com.bluairspace.sdk.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.bluairspace.sdk.R
import com.bluairspace.sdk.util.publicutil.PermissionUtil

class PermissionsDialog : DialogFragment() {

    private val onPositiveButtonClickListener = View.OnClickListener{ _ ->
        this@PermissionsDialog.dismiss()
        PermissionUtil.askPermission(this@PermissionsDialog.activity!!)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertDialogBuilder = AlertDialog.Builder(context!!)
        val view = activity!!.layoutInflater.inflate(R.layout.permissions_dialog, null) as LinearLayout
        initView(view)
        alertDialogBuilder.setView(view)
        return alertDialogBuilder.create()
    }

    private fun initView(view: View) {
        view.findViewById<Button>(R.id.permission_ok_button)?.setOnClickListener { v -> onPositiveButtonClickListener.onClick(v) }
        setTexts(view)
    }

    private fun setTexts(view: View) {
        val cameraSpannable = SpannableStringBuilder(getString(R.string.request_camera_permission))
        cameraSpannable.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.camera_permission).length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        view.findViewById<TextView>(R.id.permission_camera_text)?.text = cameraSpannable

        val locationSpannable = SpannableStringBuilder(getString(R.string.request_location_permission))
        locationSpannable.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.location_permission).length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        locationSpannable.setSpan(ForegroundColorSpan(Color.GRAY), locationSpannable.length - getString(R.string.optional_permission).length,
                locationSpannable.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        view.findViewById<TextView>(R.id.permission_location_text)?.text = locationSpannable

        val storageSpannable = SpannableStringBuilder(getString(R.string.request_storage_permission))
        storageSpannable.setSpan(StyleSpan(Typeface.BOLD), 0, getString(R.string.storage_permission).length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        storageSpannable.setSpan(ForegroundColorSpan(Color.GRAY), storageSpannable.length - getString(R.string.optional_permission).length,
            storageSpannable.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        view.findViewById<TextView>(R.id.permission_storage_text)?.text = storageSpannable
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, this.javaClass.simpleName)
    }

    companion object {

        private fun createDialog(): PermissionsDialog {
            return PermissionsDialog()
        }

        fun openPermissionsDialog(activity: FragmentActivity) {
            createDialog().show(activity.supportFragmentManager)
        }
    }
}
