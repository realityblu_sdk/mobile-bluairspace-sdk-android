package com.bluairspace.sdk.view

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.bluairspace.sdk.R


class ProofingDialog : DialogFragment() {

    private var onPositiveButtonClickListener: View.OnClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val positiveText = arguments!!.getString(POSITIVE)
        val message = arguments!!.getString(MESSAGE)
        val title = arguments!!.getString(TITLE)

        val titleSpan = SpannableString(title)
        titleSpan.setSpan(ForegroundColorSpan(Color.BLACK), 0, titleSpan.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val messageSpan = SpannableString(message)
        messageSpan.setSpan(ForegroundColorSpan(Color.BLACK), 0, messageSpan.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        return AlertDialog.Builder(activity!!)
                .setTitle(titleSpan)
                .setMessage(messageSpan)
                .setNegativeButton("Cancel") { _, _ -> dismiss() }
                .setPositiveButton(positiveText) { _, _ ->
                    if (onPositiveButtonClickListener != null) onPositiveButtonClickListener!!.onClick(view)
                }
                .create()
    }

    fun setOnPositiveButtonClickListener(clickListener: View.OnClickListener) {
        onPositiveButtonClickListener = clickListener
    }

    fun show(fragmentManager: FragmentManager, onPositiveClickListener: View.OnClickListener) {
        onPositiveButtonClickListener = onPositiveClickListener
        show(fragmentManager, this.javaClass.simpleName)
    }

    companion object {
        private const val POSITIVE = "positive"
        private const val MESSAGE = "message"
        private const val TITLE = "title"

        private const val TITLE_PROOFING = "Proofing mode"
        private const val ENTER_PROOFING_MESSAGE = "You are about to enter proofing mode"
        private const val LEAVE_PROOFING_MESSAGE = "Scanning screen will be lost.\nDo you want to leave?"

        fun createOKDialog(): ProofingDialog {
            val proofingDialog = ProofingDialog()
            val args = Bundle()
            args.putString(POSITIVE, "OK")
            args.putString(TITLE, TITLE_PROOFING)
            args.putString(MESSAGE, ENTER_PROOFING_MESSAGE)
            proofingDialog.arguments = args
            return proofingDialog
        }

        fun createLeaveDialog(): ProofingDialog {
            val proofingDialog = ProofingDialog()
            val args = Bundle()
            args.putString(POSITIVE, "Leave")
            args.putString(TITLE, TITLE_PROOFING)
            args.putString(MESSAGE, LEAVE_PROOFING_MESSAGE)
            proofingDialog.arguments = args
            return proofingDialog
        }
    }
}
