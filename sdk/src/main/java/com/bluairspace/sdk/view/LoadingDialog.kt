package com.bluairspace.sdk.view

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.bluairspace.sdk.R

class LoadingDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity!!)
                .setView(View.inflate(activity, R.layout.dialog_loading, null))
                .create()

        setStyle(STYLE_NO_TITLE, theme)
        isCancelable = false
        dialog.setCancelable(false)
        return dialog
    }

    companion object {
        val TAG = LoadingDialog::class.java.simpleName
    }
}