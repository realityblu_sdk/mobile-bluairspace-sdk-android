package com.bluairspace.sdk.helper

import com.bluairspace.sdk.model.MarkerbasedMarker
import com.bluairspace.sdk.model.MarkerlessExperience
import com.bluairspace.sdk.model.MarkerlessGroup
import com.bluairspace.sdk.model.callback.DataCallback
import com.bluairspace.sdk.network.ApiFactory
import com.bluairspace.sdk.util.publicutil.Global.APPLICATION_ID
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.getExpByIdError
import com.bluairspace.sdk.util.publicutil.Global.accountId
import com.bluairspace.sdk.util.sdk.tasks.MarkerlessExperienceFilesLoadHelper
import com.bluairspace.sdk.util.sdk.tasks.TaskHelper.loadDataAsync
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.*

/**
 * Helper for getting data. Some objects need to use Ar features
 */
object BluDataHelper {

    /**
     * Allow to get list of groups
     */
    @JvmOverloads
    fun getMarkerlessGroups(callback: DataCallback<MarkerlessGroup>, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        loadDataAsync(callback, coroutineScope) {
            val list = ApiFactory.getDataRetrofit().getSceneList(APPLICATION_ID)
            if (list.isNullOrEmpty()) return@loadDataAsync emptyList<MarkerlessGroup>()
            list.filter { it.id > 0 }
        }
    }

    /**
     * Allow to get list of experiences
     */
    @JvmOverloads
    fun getMarkerlessExperiences(groupId: Int, callback: DataCallback<MarkerlessExperience>, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        loadDataAsync(callback, coroutineScope) {
            val listGroup = ApiFactory.getDataRetrofit().getGroupList(APPLICATION_ID, groupId)
            if (listGroup.isNullOrEmpty()) return@loadDataAsync emptyList<MarkerlessExperience>()
            val list = mutableListOf<MarkerlessExperience>()
            listGroup.forEach { group ->
                group.experiences.forEach {
                    it.group = group.listName
                    it.subGroup = group.groupName
                    it.subGroupId = group.groupId
                    it.groupId = groupId
                }
                list.addAll(group.experiences)
            }
            if (listGroup.isNotEmpty()) accountId = listGroup[0].accountId
            list
        }
    }

    /**
     * Allow to get experience by Id
     */
    @JvmOverloads
    fun getMarkerlessExperienceById(expId: Int, callback: DataCallback<MarkerlessExperience>, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        loadDataAsync(callback, coroutineScope) {
            val listGroup = ApiFactory.getDataRetrofit().getMarkerlessExperienceById(APPLICATION_ID, expId)
            if (listGroup.isNullOrEmpty()) throw Throwable(getExpByIdError)
            val list: MutableList<MarkerlessExperience> = LinkedList()
            listGroup.forEach { list.addAll(it.experiences) }
            list
        }
    }

    fun cancelMarkerlessLoadingFilesTask() = MarkerlessExperienceFilesLoadHelper.cancelLoadingTask()
    fun isMarkerlessLoadingFilesTaskActive() = MarkerlessExperienceFilesLoadHelper.isTaskActive()

    /**
     * Allow to get list of markers
     */
    @JvmOverloads
    fun getMarkerbasedMarkers(callback: DataCallback<MarkerbasedMarker>, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        loadDataAsync(callback, coroutineScope) {
            ApiFactory.getDataRetrofit().getMarkerbasedTargets(APPLICATION_ID)
        }
    }
}