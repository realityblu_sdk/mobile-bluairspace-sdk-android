package com.bluairspace.sdk.helper

import android.content.Context
import androidx.fragment.app.FragmentActivity
import com.bluairspace.sdk.R
import com.bluairspace.sdk.model.MarkerBasedSettings
import com.bluairspace.sdk.model.MarkerlessExperience
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.model.exception.BluairspaceSdkException
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.permissionError
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.PermissionUtil
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import com.bluairspace.sdk.util.sdk.ArStartActivityHelper
import com.bluairspace.sdk.util.sdk.ArStartActivityHelper.updateLocation
import com.bluairspace.sdk.util.sdk.IOUtils
import com.bluairspace.sdk.util.sdk.LogSdk
import com.bluairspace.sdk.util.sdk.tasks.InitAppHelper.initSdkPath
import com.bluairspace.sdk.util.sdk.tasks.LocationHelper

/**
 * Main class of sdk. It is point of entry for using Ar features
 */
object Blu {

    private val TAG = Blu::class.java.simpleName
    private var isInitialized: Boolean = false

    /**
     * Allow to open Markerbased screen with custom errors handling
     *
     * @param activity Activity
     * @param callback Interface with results for custom errors handling
     * @param markerBasedSettings Class for init single scan or proofing mode.
     */
    fun startMarkerbased(activity: FragmentActivity, callback: TaskCallback, markerBasedSettings: MarkerBasedSettings) {
        LogSdk.d(TAG, activity.getString(R.string.log_markerbased))
        if (!PermissionUtil.isPermissionsGranted(activity)) {
            callback.onFail(BluairspaceSdkException(permissionError))
            return
        }
        AnalyticsUtil.setProofingMode(markerBasedSettings.isProofingEnabled)
        ArStartActivityHelper.prepareMarkerbasedActivity(activity, callback, markerBasedSettings)
    }

    /**
     * Allow to open Markerbased screen with custom errors handling and default settings
     * singleScan - disabled
     * proofing - disabled
     * @param activity Activity
     * @param callback Interface with results for custom errors handling
     */
    fun startMarkerbased(activity: FragmentActivity, callback: TaskCallback) {
        startMarkerbased(activity, callback, MarkerBasedSettings.defaultMarkerBasedSettings())
    }

    fun startMarkerless(activity: FragmentActivity, list: List<MarkerlessExperience>, callback: TaskCallback) {
        LogSdk.d(TAG, activity.getString(R.string.log_markerless))
        ArStartActivityHelper.prepareMarkerlessActivity(activity, list, callback)
    }

    fun startMarkerlessById(activity: FragmentActivity, id: Int, callback: TaskCallback) {
        LogSdk.d(TAG, activity.getString(R.string.log_markerless_id))
        ArStartActivityHelper.prepareMarkerlessActivityById(activity, id, callback)
    }

    /**
     * Preparation sdk. It must was perform initialization procedure before using sdk
     *
     * @param sdkKey   The key of sdk
     * @param callback Interface with results
     * @return Returns true if process initialization was started.
     */
    fun init(context: Context, sdkKey: String, callback: TaskCallback?): Boolean {
        LogSdk.d(TAG, context.getString(R.string.log_init_start, sdkKey))

        var callBack = callback
        if (callBack == null) callBack = TaskCallback.EMPTY

        if (isInitialized) {
            LogSdk.d(TAG, context.getString(R.string.log_init_early))
            callBack.onSuccess()
            return true
        }

        val appContext = context.applicationContext

        PermissionUtil.isPermissionsGranted(context)
        IOUtils.init(context)
        MainSettings.init(context)
        AnalyticsUtil.init(appContext)
        initSdkPath(callBack)

        updateLocation(context)

        Global.APPLICATION_ID = appContext.packageName
        Global.API_KEY = sdkKey

        callBack.onSuccess()
        isInitialized = true
        return true
    }

    fun clearCurrentTasks() {
        LocationHelper.removeArTasks()
    }
}
