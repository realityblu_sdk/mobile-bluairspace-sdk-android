package com.bluairspace.sdk.helper

import android.text.TextUtils
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.model.networkCall.FeedbackData
import com.bluairspace.sdk.model.networkCall.UserInfo
import com.bluairspace.sdk.network.ApiFactory
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.UrlHelper
import com.bluairspace.sdk.util.sdk.tasks.InitAppHelper
import com.bluairspace.sdk.util.sdk.tasks.TaskHelper.doTaskAsync
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

object BluUserHelper {

    @JvmOverloads
    fun sendFeedback(feedbackData: FeedbackData, callback: TaskCallback, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        doTaskAsync(callback, coroutineScope) {
            ApiFactory.getFeedbackRetrofit().sendFeedback(FeedbackData.FeedbackRequest(feedbackData))
        }
    }

    @JvmOverloads
    fun sendUserInfo(data: UserInfo, callback: TaskCallback, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        doTaskAsync(callback, coroutineScope) {
            ApiFactory.getSavePersonalInfoRetrofit().createPv(UrlHelper.endpoint.persVideoUrlUserData, data)
        }
    }

    @JvmOverloads
    fun setTrialEmail(email: String, callback: TaskCallback, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        doTaskAsync(callback, coroutineScope) {
            if (TextUtils.isEmpty(email)) {
                MainSettings.trialEmail = email
                Global.clearInit()
            }
            InitAppHelper.initSdk(email)
            MainSettings.trialEmail = email
        }
    }
}
