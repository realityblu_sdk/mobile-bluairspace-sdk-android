package com.bluairspace.sdk.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class MarkerlessExperienceGroup : Serializable {

    @SerializedName("group_id")
    var groupId: Int = -1

    @SerializedName("group_name")
    var groupName: String = ""

    @SerializedName("list_name")
    var listName: String = ""

    @SerializedName("experiences")
    var experiences: List<MarkerlessExperience> = ArrayList()

    @SerializedName("account_id")
    var accountId: Int = -1
}