package com.bluairspace.sdk.model.networkCall

import com.google.gson.annotations.SerializedName

class CurrentLocation(
        @field:SerializedName("lat")
        val latitude: Double,
        @field:SerializedName("long")
        val longitude: Double) {

    val shortString: String
        get() = "$latitude,$longitude"

    companion object {
        var EMPTY = CurrentLocation(0.0, 0.0)
    }
}
