package com.bluairspace.sdk.model.networkCall

import com.google.gson.annotations.SerializedName

internal class CommonResponse {

    @SerializedName("message")
    val message: String? = null
}
