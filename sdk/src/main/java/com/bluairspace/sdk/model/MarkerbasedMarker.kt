package com.bluairspace.sdk.model

import com.google.gson.annotations.SerializedName

class MarkerbasedMarker: SimpleItem {

    @SerializedName("id")
    override var id: Int = -1

    @SerializedName("title")
    override var title: String = ""

    @SerializedName("target_file")
    var targetFile: String = ""

    @SerializedName("icon")
    override var icon: String = ""
}
