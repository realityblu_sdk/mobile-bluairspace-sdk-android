package com.bluairspace.sdk.model.exception

internal class NotValidDataException(message: String) : Throwable(message)
