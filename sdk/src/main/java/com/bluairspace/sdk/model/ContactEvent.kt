package com.bluairspace.sdk.model

import com.google.gson.Gson

import java.text.ParseException

internal class ContactEvent {

    val name: String = ""
    val company: String = ""
    val title: String = ""
    val phoneType: String = ""
    val phone: String = ""
    val emailType: String = ""
    val email: String = ""

    companion object {

        @Throws(ParseException::class)
        fun parse(json: String): ContactEvent {
            return Gson().fromJson(json, ContactEvent::class.java)
        }
    }
}
