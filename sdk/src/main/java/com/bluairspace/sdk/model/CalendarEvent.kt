package com.bluairspace.sdk.model

import com.google.gson.Gson
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal class CalendarEvent {

    lateinit var title: String
    lateinit var description: String
    private lateinit var startDate: String
    private lateinit var endDate: String

    lateinit var startDateCalendar: Calendar
        private set
    lateinit var endDateCalendar: Calendar
        private set

    companion object {

        @JvmStatic
        val SERVER_DATE_TIME_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())

        init {
            SERVER_DATE_TIME_FORMAT.timeZone = TimeZone.getTimeZone("UTC")
        }

        @Throws(ParseException::class)
        fun parse(json: String): CalendarEvent {

            val event = Gson().fromJson(json, CalendarEvent::class.java)

            var calendar = Calendar.getInstance()
            calendar.time = SERVER_DATE_TIME_FORMAT.parse(event.startDate)!!
            event.startDateCalendar = calendar

            calendar = Calendar.getInstance()
            calendar.time = SERVER_DATE_TIME_FORMAT.parse(event.endDate)!!
            event.endDateCalendar = calendar

            return event
        }
    }
}
