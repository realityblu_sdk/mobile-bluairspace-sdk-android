package com.bluairspace.sdk.model.networkCall

import com.bluairspace.sdk.util.publicutil.Global
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class UserInfo : Serializable {

    @SerializedName("firstName")
    var firstName: String = ""

    @SerializedName("lastName")
    var lastName: String = ""

    @SerializedName("email")
    var email: String = ""

    @SerializedName("businessName")
    var business: String = ""

    @SerializedName("gender")
    var gender: Gender? = null

    @SerializedName("industry")
    var industry: Industry? = null

    @SerializedName("music")
    var music: Music? = null

    @SerializedName("appVersion")
    var appVersion = Global.VERSION_SDK_NAME

    enum class Gender(val displayName: String) : Serializable {
        Male("Male"),
        Female("Female");

        companion object {
            fun getById(id: Int): Gender {
                val list = listOf(*Gender::class.java.enumConstants!!)
                return list[id]
            }
        }
    }

    enum class Industry(val displayName: String) : Serializable {
        Healthcare("Healthcare"),
        Finance("Finance"),
        Marketing("Marketing"),
        Manufacturing("Manufacturing"),
        Entertainment("Entertainment"),
        Sports("Sports"),
        Other("Other");

        companion object {
            fun getById(id: Int): Industry {
                val list = listOf(*Industry::class.java.enumConstants!!)
                return list[id]
            }
        }
    }

    enum class Music(val displayName: String) : Serializable {
        Alternative("Alternative"),
        Classical("Classical"),
        Country("Country"),
        Randb("R&B"),
        ClassicRock("Classic Rock"),
        Jazz("Jazz");

        companion object {
            fun getById(id: Int): Music {
                val list = Arrays.asList(*Music::class.java.enumConstants!!)
                return list[id]
            }
        }
    }

    internal class UserInfoRequest(
        @field:SerializedName("user")
            private val user: UserInfo,
        @field:SerializedName("storyboard_id")
            private val storyboardId: Int,
        @field:SerializedName("experience_id")
            private val experienceId: Int) : CommonRequest()
}
