package com.bluairspace.sdk.model

class MarkerBasedSettings(
        val isProofingEnabled: Boolean,
        val isSingleScanEnabled: Boolean) {

    data class Builder(
            var isProofingEnabled: Boolean = false,
            var isSingleScanEnabled: Boolean = false) {

        fun isProofingEnabled(isProofingEnabled: Boolean) = apply { this.isProofingEnabled = isProofingEnabled }
        fun isSingleScanEnabled(isSingleScanEnabled: Boolean) = apply { this.isSingleScanEnabled = isSingleScanEnabled }
        fun build() = MarkerBasedSettings(isProofingEnabled, isSingleScanEnabled)
    }

    companion object{
        fun defaultMarkerBasedSettings(): MarkerBasedSettings{
            return Builder().build()
        }

        fun proofingMarkerBasedSettings(): MarkerBasedSettings{
            return Builder().isProofingEnabled(true).build()
        }
    }
}