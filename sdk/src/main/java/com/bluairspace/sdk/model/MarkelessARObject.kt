package com.bluairspace.sdk.model

internal class MarkelessARObject {

    internal var id: Int = -1
    internal var filePath: String = ""
    internal var iconPath: String = ""
    internal var obj_animations: String? = null
    internal var modelScale: List<Float>? = null
    internal var modelRotation: List<Float>? = null
    internal var modelPosition: List<Float>? = null
    internal var scale_mobile: Float? = null
    internal var audioFile = ""
    internal var audioLoop: Int? = 0

    fun setTransform(scale: List<Float>?, position: List<Float>?, rotation: List<Float>?, scaleMobile: Float?) {
        modelScale = scale
        modelPosition = position
        modelRotation = rotation
        scale_mobile = scaleMobile
    }

    fun setAudio(audioFile: String?, audioLoop: Int?) {
        audioFile?.let { this.audioFile = it }
        this.audioLoop = audioLoop
    }
}