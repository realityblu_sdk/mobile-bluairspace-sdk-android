package com.bluairspace.sdk.model

import java.util.*

internal class MarkerlessModelDuration(val id: Int, private var currentTime: Long) {

    private var duration: Long = 0

    fun pauseDuration(): Long {
        val time = Date().time
        duration += time - currentTime
        currentTime = time
        return duration
    }

    fun setDuration() {
        currentTime = Date().time
    }
}
