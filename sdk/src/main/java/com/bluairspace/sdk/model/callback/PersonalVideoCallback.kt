package com.bluairspace.sdk.model.callback

interface PersonalVideoCallback {
    fun sendNextVideoLoaded(id: Any, videoUrl: String)
}