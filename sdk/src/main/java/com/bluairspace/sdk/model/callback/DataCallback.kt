package com.bluairspace.sdk.model.callback

import android.util.Log
import com.bluairspace.sdk.model.exception.BluairspaceSdkException

interface DataCallback<T> {
    fun onSuccess(list: List<T>)
    fun onFail(exception: BluairspaceSdkException)

    /**
     * Do not override this method in your code
     * @see TaskCallback.onProgress
     */
    fun onProgress(currentItemCount: Int, maxItemCount: Int){
        Log.d(DataCallback::class.java.name, "Stub. Override this method for startMarkerkess()")
    }
}