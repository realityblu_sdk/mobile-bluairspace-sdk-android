package com.bluairspace.sdk.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MarkerlessExperience : Serializable {

    @SerializedName("id")
    var id: Int = -1

    @SerializedName("name")
    var name: String = ""

    @SerializedName("image")
    var image: String = ""

    @SerializedName("file_name")
    var fileName: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("icon")
    var icon: String = ""

    @SerializedName("shop_url")
    var shopUrl: String = ""

    @SerializedName("obj_animations")
    val animation: String = ""

    @SerializedName("scale")
    var scale: List<Float>? = null
        internal set

    @SerializedName("position")
    var position: List<Float>? = null
        internal set

    @SerializedName("rotation")
    var rotation: List<Float>? = null
        internal set

    @SerializedName("scale_mobile")
    var scaleMobile: Float? = null
        internal set

    @SerializedName("audio_file")
    val audioFile: String = ""

    @SerializedName("audio_loop")
    val audioLoop: Int? = null

    var group = ""
    var subGroup = ""
    var groupId = -1
    var subGroupId = -1

    var isSelected: Boolean = false
}