package com.bluairspace.sdk.model.callback

import android.util.Log
import com.bluairspace.sdk.model.exception.BluairspaceSdkException
import com.bluairspace.sdk.util.sdk.LogSdk

interface TaskCallback {

    fun onSuccess()
    fun onFail(exception: BluairspaceSdkException)

    /**
     * Override method only for [com.bluairspace.sdk.helper.Blu.startMarkerless]
     */
    fun onProgress(currentItemCount: Int, maxItemCount: Int){
        Log.d(DataCallback::class.java.name, "Stub. Override this method for Blu.startMarkerkess()")
    }

    companion object {

        val TAG: String = TaskCallback::class.java.simpleName

        val EMPTY: TaskCallback = object : TaskCallback {
            override fun onSuccess() {
                LogSdk.d(TAG, "TaskCallback EMPTY -> onSuccess()")
            }

            override fun onFail(exception: BluairspaceSdkException) {
                LogSdk.d(TAG, "TaskCallback EMPTY -> onFail(), errorMessage=${exception.message}")
            }
        }
    }
}
