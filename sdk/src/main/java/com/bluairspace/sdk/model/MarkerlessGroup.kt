package com.bluairspace.sdk.model

import com.google.gson.annotations.SerializedName

class MarkerlessGroup: SimpleItem {

    @SerializedName("id")
    override var id: Int = -1

    @SerializedName("name")
    override var title: String = ""

    @SerializedName("image")
    override var icon: String = ""
}
