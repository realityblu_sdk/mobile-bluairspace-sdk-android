package com.bluairspace.sdk.model

interface SimpleItem {
    var id: Int
    var title: String
    var icon: String
}