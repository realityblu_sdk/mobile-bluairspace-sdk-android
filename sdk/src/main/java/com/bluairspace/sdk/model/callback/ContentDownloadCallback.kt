package com.bluairspace.sdk.model.callback

import org.json.JSONObject

internal interface ContentDownloadCallback {
    fun sendEndDownloadingModelsJsonNew()
    fun sendNextFileLoaded(item: JSONObject)
}