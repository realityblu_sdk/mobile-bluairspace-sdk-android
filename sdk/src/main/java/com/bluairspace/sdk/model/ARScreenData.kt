package com.bluairspace.sdk.model

import com.bluairspace.sdk.activity.ar.ARActivity
import com.wikitude.common.camera.CameraSettings.*
import java.io.Serializable
import java.util.*

internal class ARScreenData: Serializable {

    var path: String = ""
    var activityClass = DEFAULT_ACTIVITY
    var extensions: List<String> = ArrayList()
    var cameraPosition = DEFAULT_CAMERA_POSITION
    var cameraResolution = DEFAULT_CAMERA_RESOLUTION
    var cameraFocusMode = DEFAULT_CAMERA_FOCUS_MODE
    var isCamera2Enabled = DEFAULT_CAMERA_2_ENABLED

     internal companion object {
        val DEFAULT_ACTIVITY: Class<*> = ARActivity::class.java
        val DEFAULT_CAMERA_POSITION = CameraPosition.DEFAULT
        val DEFAULT_CAMERA_RESOLUTION = CameraResolution.SD_640x480
        val DEFAULT_CAMERA_FOCUS_MODE = CameraFocusMode.CONTINUOUS
        const val DEFAULT_CAMERA_2_ENABLED = true
    }
}
