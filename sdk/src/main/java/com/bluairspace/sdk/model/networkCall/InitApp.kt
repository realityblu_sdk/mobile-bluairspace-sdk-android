package com.bluairspace.sdk.model.networkCall

import com.google.gson.annotations.SerializedName

internal class InitApp {

    class ResponseInitApp : CommonRequest() {
        @SerializedName("error")
        var error: Int? = null

        @SerializedName("error_message")
        var errorMessage: String = ""

        @SerializedName("data")
        var data: Data? = null
    }

    inner class Data {

        @SerializedName("client_token")
        val clientToken: String = ""

        @SerializedName("wikitude_collection")
        val arCollection: String = ""

        @SerializedName("proofing_wikitude_collection")
        val proofingARCollection: String = ""

        @SerializedName("version")
        val version: Int = -1

        @SerializedName("ar_key")
        val arKey: String = ""

        @SerializedName("group_id")
        val groupId: String = ""
    }
}
