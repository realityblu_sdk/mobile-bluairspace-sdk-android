package com.bluairspace.sdk.model

enum class ArType(val value: Int) {
    INSTANTTRACKING(0), CLOUDRECOGNITION(1), PROOFING(2);

    companion object {
        fun valueOf(value: Int): ArType {
            return when (value) {
                0 -> INSTANTTRACKING
                1 -> PROOFING
                2 -> CLOUDRECOGNITION
                else -> throw IllegalArgumentException("No such Ar type")
            }
        }
    }
}