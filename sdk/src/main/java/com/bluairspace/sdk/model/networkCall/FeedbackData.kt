package com.bluairspace.sdk.model.networkCall

import com.google.gson.annotations.SerializedName

class FeedbackData : CommonRequest() {

    @SerializedName("name")
    var name: String = ""

    @SerializedName("email")
    var email: String = ""

    @SerializedName("phone")
    var phone: String = ""

    @SerializedName("content")
    var comments: String = ""

    @SerializedName("app_version")
    var appVersion: String = ""

    @SerializedName("os")
    var os: String = ""

    @SerializedName("appId")
    var appId: String = ""

    internal class FeedbackRequest(
        @field:SerializedName("request")
        private val user: FeedbackData): CommonRequest()
}
