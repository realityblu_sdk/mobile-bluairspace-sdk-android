package com.bluairspace.sdk.util.sdk.tasks

import android.text.TextUtils
import android.util.Log
import com.bluairspace.sdk.model.networkCall.UserInfo
import com.bluairspace.sdk.model.callback.PersonalVideoCallback
import com.bluairspace.sdk.network.ApiFactory
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.UrlHelper
import com.bluairspace.sdk.util.sdk.LogSdk
import com.google.gson.Gson
import kotlinx.coroutines.*
import org.json.JSONObject

internal object PersonalVideoDownloadHelper {

    private const val FIELD_EXP_ID = "experience_id"
    private const val FIELD_VIDEO_ARRAY = "personal_videos"
    private const val FIELD_VIDEO_ID = "id"
    private const val FIELD_STORYBOARD_ID = "storyboardId"
    private val TAG = PersonalVideoDownloadHelper::class.java.simpleName

    private lateinit var job: Job

    internal fun cancelCurrentJob() {
        if (::job.isInitialized && job.isActive) job.cancel()
    }

    internal fun startDownload(jsonObject: JSONObject, personalVideoCallback: PersonalVideoCallback) {
        if (!::job.isInitialized || !job.isActive) startLoading(jsonObject, personalVideoCallback)
        else Log.d(TAG, "Warning, attempt to start download content twice")
    }

    private fun startLoading(jsonObject: JSONObject, personalVideoCallback: PersonalVideoCallback) {
        job = CoroutineScope(Dispatchers.IO).launch {
            LogSdk.d(TAG, "inputJson=$jsonObject")

            val json = MainSettings.personalVideoData
            var user = UserInfo()
            try {
                if (!TextUtils.isEmpty(json)) user = Gson().fromJson(json, UserInfo::class.java)
                var experienceId = -1
                if (jsonObject.has(FIELD_EXP_ID)) experienceId = jsonObject.getInt(FIELD_EXP_ID)

                val videos = jsonObject.getJSONArray(FIELD_VIDEO_ARRAY)
                for (i in 0 until videos.length()) {
                    val jObject = videos.getJSONObject(i)
                    LogSdk.d(TAG, "handle video array, item=$i, jObject=$jObject")
                    val id = jObject.get(FIELD_VIDEO_ID)
                    val storyboardId = jObject.getInt(FIELD_STORYBOARD_ID)
                    val url = getVideoUrlFromServer(user, storyboardId, experienceId)
                    withContext(Dispatchers.Main) {
                        if (!TextUtils.isEmpty(url) && url.length > 5) personalVideoCallback.sendNextVideoLoaded(id, url)
                        else personalVideoCallback.sendNextVideoLoaded(id, "")
                    }
                }
                LogSdk.v(TAG, "generate video task is successful")
            } catch (e: Throwable) {
                e.printStackTrace()
                LogSdk.v(TAG, "generate video task is not successful")
                LogSdk.v(TAG, e.message.toString())
            }
        }
    }

    private suspend fun getVideoUrlFromServer(user: UserInfo, storyboardId: Int, expId: Int): String {
        val request = UserInfo.UserInfoRequest(user, storyboardId, expId)
        val response = ApiFactory.getLoadPersonalInfoRetrofit().getPersonVideoForMobile(UrlHelper.endpoint.persVideoUrlGenerateVideo, request)
        val result = response.string().replace("\"".toRegex(), "")
        LogSdk.d(TAG, "VideoUrlfromServer=$result")
        return result
    }
}
