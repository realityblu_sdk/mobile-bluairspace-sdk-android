package com.bluairspace.sdk.util.sdk.tasks

import com.bluairspace.sdk.model.callback.DataCallback
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.model.exception.BluairspaceSdkException
import com.bluairspace.sdk.util.sdk.ConnectionUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

internal object TaskHelper {

    //TODO: uncomment it after update coroutines
    internal /*inline*/ fun <T> loadDataAsync(callback: DataCallback<T>, coroutineScope: CoroutineScope, /*crossinline*/ doOnAsyncBlock: suspend CoroutineScope.() -> List<T>) =
        coroutineScope.launch {
            try {
                prepareCall()
                val result = doOnAsyncBlock.invoke(this)
                withContext(Dispatchers.Main) { callback.onSuccess(result) }
            } catch (e: Throwable) {
                e.printStackTrace()
                withContext(Dispatchers.Main) { callback.onFail(BluairspaceSdkException(e.message.toString(), e.cause)) }
            }
        }

    //TODO: uncomment it after update coroutines
    internal /*inline*/ fun doTaskAsync(callback: TaskCallback, coroutineScope: CoroutineScope, /*crossinline*/ doOnAsyncBlock: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch {
            try {
                prepareCall()
                doOnAsyncBlock.invoke(this)
                withContext(Dispatchers.Main) { callback.onSuccess() }
            } catch (e: Throwable) {
                e.printStackTrace()
                withContext(Dispatchers.Main) { callback.onFail(BluairspaceSdkException(e.message.toString(), e.cause)) }
            }
        }
    }

    private suspend fun prepareCall() {
        ConnectionUtils.isInitSuccess()
    }
}