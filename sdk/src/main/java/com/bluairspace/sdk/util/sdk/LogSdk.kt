package com.bluairspace.sdk.util.sdk

import android.util.Log
import com.bluairspace.sdk.util.publicutil.Global

internal object LogSdk {

    private const val TAG = "BluSDK"

    fun d(tag: String, message: String) {
        if (Global.DEBUG) {
            Log.d(TAG, String.format("%s -> %s", tag, message))
        }
    }

    fun v(tag: String, message: String) {
        Log.v(TAG, String.format("%s -> %s", tag, message))
    }
}
