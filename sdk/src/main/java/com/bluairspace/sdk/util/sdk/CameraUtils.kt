@file:Suppress("DEPRECATION")

package com.bluairspace.sdk.util.sdk

import android.content.Context
import android.hardware.Camera
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Build

internal object CameraUtils {

    @Throws(CameraAccessException::class)
    fun turnFlashLight(context: Context, turnOn: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val camManager: CameraManager? = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            val cameraId: String
            camManager?.let {
                cameraId = camManager.cameraIdList[0]
                camManager.setTorchMode(cameraId, turnOn)
            }
        } else {
            val camera = Camera.open()
            val parameters = camera.parameters
            parameters.flashMode = Camera.Parameters.FLASH_MODE_TORCH
            camera.parameters = parameters
            if (turnOn) camera.startPreview()
            else camera.stopPreview()
        }
    }

    @Throws(CameraAccessException::class)
    fun turnFlashLightOn(context: Context) {
        turnFlashLight(context, true)
    }

    @Throws(CameraAccessException::class)
    fun turnFlashLightOff(context: Context) {
        turnFlashLight(context, false)
    }
}
