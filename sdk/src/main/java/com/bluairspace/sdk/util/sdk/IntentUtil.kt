package com.bluairspace.sdk.util.sdk

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.CalendarContract
import android.provider.ContactsContract
import com.bluairspace.sdk.model.CalendarEvent
import com.bluairspace.sdk.model.ContactEvent
import com.bluairspace.sdk.model.exception.NotValidDataException
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.emailPatternError
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.phonePatternError
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.phonePrefix
import org.json.JSONObject
import java.util.*

internal object IntentUtil {

    private const val EMAIL = "emailTo"
    private const val SUBJECT = "emailSubject"

    @Throws(NotValidDataException::class)
    fun phoneCallManual(context: Context, phoneNumber: String) {
        var phone = phoneNumber
        phone = phone.toLowerCase(Locale.getDefault())

        if (phone.contains(phonePrefix)) phone = phone.replace(phonePrefix, "")
        if (!DataChecker.isPhoneNumber(phone)) throw NotValidDataException(phonePatternError)
        if (!phone.contains(phonePrefix)) phone = phonePrefix + phone

        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(phone)
        startIntentSafely(context, intent)
    }

    @Throws(NotValidDataException::class)
    fun emailManual(context: Context, mail: String) {
        val email: String
        val subject: String
        val emailSubject = JSONObject(mail)
        email = emailSubject.getString(EMAIL)
        subject = emailSubject.getString(SUBJECT)
        if (email.isEmpty() && !DataChecker.isEmail(mail)) throw NotValidDataException(emailPatternError)

        val intent = Intent(Intent.ACTION_SENDTO)
        val mailto = "mailto:" + email + "?&subject=" + Uri.encode(subject)
        intent.data = Uri.parse(mailto)
        startIntentSafely(context, intent)
    }

    fun addEventToCalendar(context: Context, event: CalendarEvent) {
        val intent = Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI)
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.startDateCalendar.timeInMillis)
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.endDateCalendar.timeInMillis)
        intent.putExtra(CalendarContract.Events.TITLE, event.title)
        intent.putExtra(CalendarContract.Events.DESCRIPTION, event.description)
        startIntentSafely(context, intent)
    }

    fun addContact(context: Context, contactEvent: ContactEvent) {
        val intent = Intent(Intent.ACTION_INSERT)
        intent.type = ContactsContract.Contacts.CONTENT_TYPE

        intent.putExtra(ContactsContract.Intents.Insert.NAME, contactEvent.name)
        intent.putExtra(ContactsContract.Intents.Insert.COMPANY, contactEvent.company)
        intent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, contactEvent.title)
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, contactEvent.phone)
        intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, contactEvent.phoneType)
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, contactEvent.email)
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, contactEvent.emailType)
        context.startActivity(intent)
    }

    private fun startIntentSafely(context: Context, intent: Intent) {
        var currentIntent = intent
        if (isIntentSafe(context, currentIntent)) context.startActivity(currentIntent)
        else {
            currentIntent = Intent.createChooser(currentIntent, "")
            context.startActivity(currentIntent)
        }
    }

    private fun isIntentSafe(context: Context, intent: Intent): Boolean {
        val packageManager = context.packageManager
        val activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return activities.size > 0
    }
}
