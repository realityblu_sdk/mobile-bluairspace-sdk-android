package com.bluairspace.sdk.util.sdk

import android.text.TextUtils
import android.util.Patterns

internal object DataChecker {

    fun isPhoneNumber(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) false
        else Patterns.PHONE.matcher(target).matches()
    }

    fun isEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) false
        else Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}
