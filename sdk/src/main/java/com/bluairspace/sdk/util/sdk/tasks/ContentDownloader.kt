package com.bluairspace.sdk.util.sdk.tasks

import android.net.Uri
import android.text.TextUtils
import android.util.Log
import com.bluairspace.sdk.model.callback.ContentDownloadCallback
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import com.bluairspace.sdk.util.sdk.IOUtils
import com.bluairspace.sdk.util.sdk.LogSdk
import kotlinx.coroutines.*
import org.json.JSONObject
import java.io.File
import java.util.*

internal object ContentDownloader {

    private const val FIELD_URL = "url"
    private const val FIELD_FILE_PATH = "file_path"
    private const val FIELD_MARKER_ID = "marker_id"
    private const val FIELD_UNIQUE_ID = "uniqueID"
    private const val FIELD_ITEM = "item"
    private const val FIELD_SERVER_ID = "serverID"
    private const val FIELD_EXPERIENCE_ID = "expId"
    private const val FIELD_SNAPPED = "snapped"
    private val TAG = ContentDownloader::class.java.simpleName
    private var wasDownloadedModel = false

    private lateinit var job: Job

    internal fun cancelCurrentJob() {
        if (::job.isInitialized && job.isActive) job.cancel()
    }

    internal fun startDownload(jsonObject: JSONObject, contentDownloadCallback: ContentDownloadCallback) {
        if (!ContentDownloader::job.isInitialized || !job.isActive) startLoading(jsonObject, contentDownloadCallback)
        else Log.d(TAG, "Warning, attempt to start download content twice")
    }

    private fun startLoading(jsonObject: JSONObject, contentDownloadCallback: ContentDownloadCallback) {
        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val markerId = getMarkerId(jsonObject)
                val expId = getExpId(jsonObject)
                handlingScanningMarker(jsonObject, markerId, expId)

                downloadingCollection(jsonObject, "markers", contentDownloadCallback)
                downloadingCollection(jsonObject, "buttons", contentDownloadCallback)
                downloadingCollection(jsonObject, "video", contentDownloadCallback)
                downloadingCollection(jsonObject, "models", contentDownloadCallback)
                downloadingCollection(jsonObject, "images", contentDownloadCallback)

                if (wasDownloadedModel) {
                    AnalyticsUtil.faSendMarkerbasedClick(markerId, expId)
                    wasDownloadedModel = false
                }

                withContext(Dispatchers.Main) {
                    LogSdk.v(TAG, "downloading task is successful")
                    contentDownloadCallback.sendEndDownloadingModelsJsonNew()
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                LogSdk.v(TAG, "downloading task is not successful")
                e.message?.let { LogSdk.v(TAG, it) }
            }
        }
    }

    private fun downloadingCollection(jsonObject: JSONObject, nameCollection: String, contentDownloadCallback: ContentDownloadCallback) {
        if (jsonObject.isNull(nameCollection)) return

        val models = jsonObject.getJSONArray(nameCollection)
        for (i in 0 until models.length()) {
            val jObject = models.getJSONObject(i)

            val filePath = jObject.getString(FIELD_FILE_PATH)
            if (TextUtils.isEmpty(filePath) || "null".equals(filePath, ignoreCase = true)) {
                val name = jObject.getString(FIELD_URL)
                var url = name
                var uniqueID: Any = ""

                if (jObject.has(FIELD_UNIQUE_ID)) uniqueID = jObject.get(FIELD_UNIQUE_ID)

                if ("models".equals(nameCollection, ignoreCase = true)) {
                    val extension = IOUtils.getFileExtention(url)
                    if (extension.toLowerCase(Locale.getDefault()) == "fbx") {
                        url = url.replace(".$extension", ".wt3")
                        LogSdk.d(TAG, "model url $url")
                    }
                }

                if ("video".equals(nameCollection, ignoreCase = true)) {
                    jObject.put(FIELD_FILE_PATH, url)
                    continue
                }

                if (!TextUtils.isEmpty(url)) {
                    val resultDownloading = MarkerlessDownloadHelper.downloadFile(url)

                    if (!TextUtils.isEmpty(resultDownloading.second)) {
                        val file = File(resultDownloading.second)
                        val localFilePath = Uri.decode(Uri.fromFile(file).toString())
                        jObject.put(FIELD_FILE_PATH, localFilePath)

                        if (!"markers".equals(nameCollection, ignoreCase = true)) {
                            val item = createItem(nameCollection, name, uniqueID, localFilePath)
                            val loadedItem = createLoadedItem(item)
                            contentDownloadCallback.sendNextFileLoaded(loadedItem)
                        }

                        if (resultDownloading.first) wasDownloadedModel = true

                    } else LogSdk.v(TAG, "file not found url=$url")
                }
            }
        }
    }

    private fun getExpId(jsonObject: JSONObject): Int {
        if (!jsonObject.isNull(FIELD_EXPERIENCE_ID)) {
            return jsonObject.getInt(FIELD_EXPERIENCE_ID)
        }
        return -1
    }

    private fun handlingScanningMarker(jsonObject: JSONObject, markerId: Int, expId: Int) {
        if (!jsonObject.isNull(FIELD_SNAPPED)) {
            val isSnapped = jsonObject.getBoolean(FIELD_SNAPPED)
            if (!isSnapped) AnalyticsUtil.faSendMarkerbasedRecognized(markerId, expId)
        }
    }

    private fun getMarkerId(jsonObject: JSONObject): Int {
        val models = jsonObject.getJSONArray("markers")
        var markerId = -1
        for (i in 0 until models.length()) {
            val jObject = models.getJSONObject(i)
            if (jObject.has(FIELD_ITEM)) {
                val item = jObject.getJSONObject(FIELD_ITEM)
                when {
                    item.has(FIELD_MARKER_ID) -> markerId = item.getInt(FIELD_MARKER_ID)
                    item.has(FIELD_SERVER_ID) -> markerId = item.getInt(FIELD_SERVER_ID)
                    else -> LogSdk.v(TAG, "Unknown marker")
                }
            }
        }
        return markerId
    }

    private fun createItem(type: String, nameExp: String, uniqueID: Any, file_path_1: String): JSONObject {
        val data = JSONObject().apply {
            put("name", nameExp)
            put("uniqueID", uniqueID)
            put("file_path_1", file_path_1)
        }
        return JSONObject().apply {
            put("type", type)
            put("data", data)
        }
    }

    private fun createLoadedItem(item: JSONObject): JSONObject {
        val loadedItem = JSONObject()
        loadedItem.put("loadedItem", item)
        return loadedItem
    }
}
