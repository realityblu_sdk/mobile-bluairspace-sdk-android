package com.bluairspace.sdk.util.publicutil

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import androidx.preference.PreferenceManager
import com.bluairspace.sdk.network.endpoints.Endpoint
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import java.io.File
import java.lang.ref.WeakReference

object MainSettings {

    private const val MARKERBASED_TUTORIAL_SETTINGS = "setting_markerbased_tutorial"
    private const val TRIAL_EMAIL_SETTINGS = "setting_email_user_self"
    private const val AR_SETTINGS = "arlibrary"
    private const val CLOUD_TOKEN_SETTINGS = "cloud_recognition_client_token"
    private const val CLOUD_COLLECTION_SETTINGS = "cloud_recognition_ar_collection"
    private const val CLOUD_COLLECTION_PROOFING_SETTINGS = "cloud_recognition_proofing_ar_collection"
    private const val CLOUD_GROUP_SETTINGS = "cloud_recognition_group_id"
    private const val INSTANT_TRACKING_STATUS_SETTINGS = "instant_tracking_text_status"
    private const val USER_INFO_SETTINGS = "setting_pers_video_data"

    private lateinit var contextRef: WeakReference<Context>

    fun init(context: Context) {
        if (!MainSettings::contextRef.isInitialized) contextRef = WeakReference(context)
    }

    val deviceId: String
        @SuppressLint("HardwareIds")
        get() = Settings.Secure.getString(contextRef.get()!!.contentResolver, Settings.Secure.ANDROID_ID)

    var arFolderPath: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(AR_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(AR_SETTINGS, value)
                    .apply()
        }

    var cloudRecognitionClientToken: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(CLOUD_TOKEN_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(CLOUD_TOKEN_SETTINGS, value)
                    .apply()
        }

    var cloudRecognitionARCollection: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(CLOUD_COLLECTION_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(CLOUD_COLLECTION_SETTINGS, value)
                    .apply()
        }

    var cloudRecognitionProofingARCollection: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(CLOUD_COLLECTION_PROOFING_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(CLOUD_COLLECTION_PROOFING_SETTINGS, value)
                    .apply()
        }

    var cloudRecognitionGroupId: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(CLOUD_GROUP_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(CLOUD_GROUP_SETTINGS, value)
                    .apply()
        }

    var isInstantTrackingTextStatus: Boolean
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getBoolean(INSTANT_TRACKING_STATUS_SETTINGS, true)
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putBoolean(INSTANT_TRACKING_STATUS_SETTINGS, value)
                    .apply()
        }

    var personalVideoData: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(USER_INFO_SETTINGS, "")!!
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putString(USER_INFO_SETTINGS, value)
                    .apply()
        }

    var trialEmail: String
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                .getString(TRIAL_EMAIL_SETTINGS, "")!!
        set(value) = PreferenceManager.getDefaultSharedPreferences(contextRef.get()).edit().putString(
            TRIAL_EMAIL_SETTINGS, value).apply()

    var showMarkerbasedTutorial: Boolean
        get() = PreferenceManager.getDefaultSharedPreferences(contextRef.get()).getBoolean(
            MARKERBASED_TUTORIAL_SETTINGS, true)
        set(value) {
            PreferenceManager.getDefaultSharedPreferences(contextRef.get())
                    .edit()
                    .putBoolean(MARKERBASED_TUTORIAL_SETTINGS, value)
                    .apply()
        }

    fun setEnvironment(endpoint: Endpoint) {
        AnalyticsUtil.setEnvironment(endpoint)
    }

    //Delete directory recursively, need for cache clearing
    fun clearCache(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children!!.indices) {
                val success = clearCache(
                    File(
                        dir,
                        children[i]
                    )
                )
                if (!success) {
                    return false
                }
            }
            return dir.delete()
        } else return if (dir != null && dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }
}

