package com.bluairspace.sdk.util.sdk.tasks

import android.util.Pair
import com.bluairspace.sdk.util.sdk.ConnectionUtils
import com.bluairspace.sdk.util.sdk.IOUtils
import com.bluairspace.sdk.util.sdk.LogSdk
import java.io.File
import java.io.FileOutputStream
import java.net.URL

internal object MarkerlessDownloadHelper {

    private val TAG = MarkerlessDownloadHelper::class.java.simpleName

    fun downloadFile(url: String): Pair<Boolean, String> {
        LogSdk.v(TAG, "downloading $url")
        var fileName = IOUtils.getFileNamefromUrl(url)
        fileName = fileName.replace("%", "")
        File(IOUtils.externalFilesDir).mkdir()
        val file = File(IOUtils.externalFilesDir + fileName)
        return downloadFileIfNeed(url, file)
    }

    private fun downloadFileIfNeed(url: String, file: File): Pair<Boolean, String> {
        val fileSize = file.length().toInt()
        val downloadConnection = URL(url)
        val downloadFileSize = downloadConnection.openConnection().contentLength
        if (fileSize > 0 && fileSize == downloadFileSize) {
            LogSdk.d(TAG, "result - {false, " + file.absolutePath + "}")
            return Pair(false, file.absolutePath)
        } else {
            return if (ConnectionUtils.isInternetAvailable()) {
                LogSdk.d(TAG, "internet is available")
                downloadConnection.openStream().use { input ->
                    FileOutputStream(file).use { output ->
                        input.copyTo(output)
                    }
                }
                LogSdk.d(TAG, "result - {true, " + file.absolutePath + "}")
                Pair(true, file.absolutePath)
            } else {
                LogSdk.d(TAG, "result  - {false, filepath is empty}")
                Pair(false, String())
            }
        }
    }
}
