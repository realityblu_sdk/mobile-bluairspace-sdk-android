package com.bluairspace.sdk.util.publicutil

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.bluairspace.sdk.R
import com.google.android.material.snackbar.Snackbar

object PermissionUtil {

    private const val SDK_PERMISSION = 100
    const val SETTINGS_REQUEST = 101

    private val permissionList = arrayOf(Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private var snackBarTextColor = android.R.color.black
    private var snackBarBackgroundColor = android.R.color.white

    fun isPermissionsGranted(context: Context): Boolean = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

    fun askPermission(activity: FragmentActivity) = ActivityCompat.requestPermissions(activity, permissionList, SDK_PERMISSION)

    /**
     * Use this method in [Activity.onRequestPermissionsResult] and if result is true then call code you needed
     *
     * @param activity     where permissions access required
     * @param requestCode  always use [PermissionUtil.SDK_PERMISSION]
     * @param grantResults array of granted results
     * @return true if all permissions is granted
     */
    fun isPermissionsGrantedAndAsk(activity: Activity, requestCode: Int, grantResults: IntArray): Boolean {
        for (i in grantResults.indices) {
            if (permissionList[i] == Manifest.permission.ACCESS_FINE_LOCATION
                || permissionList[i] == Manifest.permission.ACCESS_COARSE_LOCATION
                || permissionList[i] == Manifest.permission.WRITE_EXTERNAL_STORAGE)
                return if (i == grantResults.size - 1)
                    true
                else
                    continue
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) break
            if (i == grantResults.size - 1) return true
        }
        if (requestCode == SDK_PERMISSION) {
            var i = 0
            val len = permissionList.size
            while (i < len) {
                val permission = permissionList[i]
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        val showRationale = activity.shouldShowRequestPermissionRationale(permission)
                        if (!showRationale) {
                            openSnackBar(activity)
                            return false
                        } else
                            return isPermissionsGranted(activity)
                    } else
                        openSnackBar(activity)
                }
                i++
            }
        }
        return false
    }

    /**
     * Set text color of snackBar settings message (default: black).
     *
     * @param color
     */
    fun setSnackBarTextColor(color: Int) {
        snackBarTextColor = color
    }

    /**
     * Set background color of snackBar settings message (default: white).
     *
     * @param color
     */
    fun setSnackBarBackgroundColor(color: Int) {
        snackBarBackgroundColor = color
    }

    private fun openSnackBar(activity: Activity) {
        val snackbar = Snackbar
                .make(activity.window.decorView.findViewById(android.R.id.content), activity.getText(R.string.grant_permissions), Snackbar.LENGTH_LONG)
                .setAction("Settings") { _ -> openSettingsScreen(activity) }
        snackbar.view.setBackgroundColor(ContextCompat.getColor(activity, snackBarBackgroundColor))
        (snackbar.view.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView).setTextColor(ContextCompat.getColor(activity, snackBarTextColor))
        snackbar.show()
    }

    private fun openSettingsScreen(activity: Activity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivityForResult(intent, SETTINGS_REQUEST)
    }
}
