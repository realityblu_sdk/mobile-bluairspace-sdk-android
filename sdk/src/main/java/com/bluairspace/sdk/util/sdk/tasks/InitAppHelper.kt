package com.bluairspace.sdk.util.sdk.tasks

import android.text.TextUtils
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.network.ApiFactory
import com.bluairspace.sdk.util.publicutil.Global.API_KEY
import com.bluairspace.sdk.util.publicutil.Global.APPLICATION_ID
import com.bluairspace.sdk.util.publicutil.Global.AR_KEY
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.arInitError
import com.bluairspace.sdk.util.publicutil.Global.ConstantsSdk.userNotFoundError
import com.bluairspace.sdk.util.publicutil.Global.INIT_SUCCESSFUL
import com.bluairspace.sdk.util.publicutil.Global.VERSION_SDK_CODE
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.sdk.ConnectionUtils
import com.bluairspace.sdk.util.sdk.LogSdk
import com.bluairspace.sdk.util.sdk.tasks.TaskHelper.doTaskAsync
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.io.File
import java.io.IOException
import java.util.*

internal object InitAppHelper{

    private val TAG = InitAppHelper::class.java.simpleName

    internal fun initSdk(taskCallback: TaskCallback, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        doTaskAsync(taskCallback, coroutineScope) {
            initSdk(MainSettings.trialEmail)
        }
    }

    internal suspend fun initSdk(trialEmail: String) {
        if (!ConnectionUtils.isInternetAvailable()) throw IOException("Check connection to internet")

        val headers = HashMap<String, String>()
        headers["api-key"] = API_KEY
        headers["sdk-version"] = "" + VERSION_SDK_CODE
        if (!TextUtils.isEmpty(trialEmail)) headers["email-user-self"] = trialEmail

        val response = ApiFactory.getInitRetrofit().getInit(headers, APPLICATION_ID) ?: throw IOException(arInitError + "Response from server is empty")
        if (response.error == null) throw IOException(arInitError + "Format of response is wrong")
        if (response.error != 0) {
            if (response.error == 7) throw IOException(userNotFoundError)
            else throw IOException(arInitError + response.errorMessage)
        }

        if (response.data == null) throw IOException(arInitError + "Response data from server is empty")
        val initAppData = response.data!!

        val clientToken = initAppData.clientToken
        val wikitudeCollection = initAppData.arCollection
        val proofingWikitudeCollection = initAppData.proofingARCollection
        if (TextUtils.isEmpty(clientToken) || TextUtils.isEmpty(wikitudeCollection) && TextUtils.isEmpty(proofingWikitudeCollection)) {
            throw IOException(arInitError + "AR license key is missing.")
        }
        MainSettings.cloudRecognitionClientToken = initAppData.clientToken
        MainSettings.cloudRecognitionARCollection = initAppData.arCollection
        MainSettings.cloudRecognitionProofingARCollection = initAppData.proofingARCollection
        MainSettings.cloudRecognitionGroupId = initAppData.groupId

        if (!TextUtils.isEmpty(initAppData.arKey)) AR_KEY = initAppData.arKey
        else {
            INIT_SUCCESSFUL = false
            throw IOException(arInitError + "AR license key is missing.")
        }
        INIT_SUCCESSFUL = true
    }

    internal fun initSdkPath(callback: TaskCallback) {
        LogSdk.d(TAG, "prepareSdkPath -> start")
        MainSettings.arFolderPath = "arlibrary" + File.separator
        callback.onSuccess()
    }
}
