package com.bluairspace.sdk.util.sdk

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.bluairspace.sdk.R
import com.bluairspace.sdk.helper.BluDataHelper
import com.bluairspace.sdk.model.*
import com.bluairspace.sdk.model.callback.DataCallback
import com.bluairspace.sdk.model.callback.TaskCallback
import com.bluairspace.sdk.model.exception.BluairspaceSdkException
import com.bluairspace.sdk.model.networkCall.CurrentLocation
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.PermissionUtil
import com.bluairspace.sdk.util.sdk.tasks.InitAppHelper
import com.bluairspace.sdk.util.sdk.tasks.LocationHelper
import com.bluairspace.sdk.util.sdk.tasks.MarkerlessExperienceFilesLoadHelper
import com.google.ar.core.ArCoreApk
import com.google.gson.Gson
import java.util.*

internal object ArStartActivityHelper {

    private val DATA_FILE_NAME = "data.json"
    private val TAG = ArStartActivityHelper::class.java.simpleName

    private lateinit var arScreensList: List<ARScreenData>
    private var blockTwiceCall = false

    private var installRequested: Boolean = false

    private val dataPath: String
        get() = MainSettings.arFolderPath + DATA_FILE_NAME

    private fun startArActivity(activity: FragmentActivity, typeValue: Int, extension: List<Any>, markerBasedSettings: MarkerBasedSettings, taskCallback: TaskCallback) {
        val arScreenData = arScreensList[typeValue]

        if (PermissionUtil.isPermissionsGranted(activity)) {
            val intent = Intent(activity, arScreenData.activityClass)
            intent.putExtra(activity.getString(R.string.param_sample_data), arScreenData)
            intent.putExtra(activity.getString(R.string.param_extra_artype), typeValue)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra(activity.getString(R.string.param_proofing), markerBasedSettings.isProofingEnabled)
            intent.putExtra(activity.getString(R.string.param_single_scan), markerBasedSettings.isSingleScanEnabled)

            for (obj in extension) {
                if (obj is String) intent.putExtra(activity.getString(R.string.param_item_string), obj)
            }
            taskCallback.onSuccess()
            activity.startActivityForResult(intent, 1)
        }
    }

    internal fun prepareMarkerbasedActivity(activity: FragmentActivity, callback: TaskCallback, markerBasedSettings: MarkerBasedSettings) {
        prepareArSdk(activity)
        InitAppHelper.initSdk(object : TaskCallback {
            override fun onSuccess() {
                startArActivity(activity, ArType.CLOUDRECOGNITION.value, ArrayList(), markerBasedSettings, callback)
            }

            override fun onFail(exception: BluairspaceSdkException) {
                callback.onFail(exception)
            }
        })
    }

    internal fun prepareMarkerlessActivity(activity: FragmentActivity, list: List<MarkerlessExperience>, callback: TaskCallback) {
        if (!isArReady(activity, callback)) return
        updateLocation(activity)
        if (list.size > Global.MAX_COUNT_MARKERLESS_MODELS) {
            callback.onFail(BluairspaceSdkException("List size can't being more 5"))
            return
        }
        MarkerlessExperienceFilesLoadHelper.loadMarkerlessExperienceFiles(list, object : DataCallback<MarkelessARObject> {
            override fun onSuccess(list: List<MarkelessARObject>) {
                prepareArSdk(activity)
                val listExtension = ArrayList<Any>()
                listExtension.add(Gson().toJson(list))
                startMarkerlessActivity(activity, ArType.INSTANTTRACKING.value, listExtension, callback)
            }

            override fun onFail(exception: BluairspaceSdkException) {
                callback.onFail(exception)
            }

            override fun onProgress(currentItemCount: Int, maxItemCount: Int) {
                callback.onProgress(currentItemCount, maxItemCount)
            }
        })
    }

    internal fun prepareMarkerlessActivityById(activity: FragmentActivity, id: Int, callback: TaskCallback) {
        if (!isArReady(activity, callback)) return
        updateLocation(activity)
        BluDataHelper.getMarkerlessExperienceById(id, object : DataCallback<MarkerlessExperience>{
            override fun onSuccess(list: List<MarkerlessExperience>) {
                prepareMarkerlessActivity(activity, list, callback)
            }

            override fun onFail(exception: BluairspaceSdkException) {
                callback.onFail(exception)
            }
        })
    }

    private fun startMarkerlessActivity(activity: FragmentActivity, childPosition: Int, extension: List<Any>, taskCallback: TaskCallback) {
        if (blockTwiceCall) { return }
        else {
            startArActivity(activity, childPosition, extension, MarkerBasedSettings.defaultMarkerBasedSettings(), taskCallback)
            blockTwiceCall = true
            Handler().postDelayed({ blockTwiceCall = false }, 2000)
        }
    }

    private fun prepareArSdk(context: Context) {
        LogSdk.v(TAG, "prepareArSdk")
        if (!ArStartActivityHelper::arScreensList.isInitialized) arScreensList = ARJsonParser.getArScreensFromJsonString(
            ARJsonParser.loadStringFromAssets(context,
            dataPath
        ))
    }

    private fun isArReady(activity: FragmentActivity, callback: TaskCallback): Boolean {
        if (!checkCompassSupport(activity)) {
            callback.onFail(BluairspaceSdkException(Global.ConstantsSdk.compassSupportError))
            return false
        }
        if (!PermissionUtil.isPermissionsGranted(activity)) {
            callback.onFail(BluairspaceSdkException(Global.ConstantsSdk.permissionError))
            return false
        }
        if (!checkArSupport(activity)) {
            callback.onFail(BluairspaceSdkException(Global.ConstantsSdk.arCoreSupportError))
            return false
        }
        return true
    }

    internal fun updateLocation(context: Context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        LocationHelper.getLocation(context, (3 * 1000).toLong(), object : LocationHelper.LocationResult() {
            override fun gotLocation(location: CurrentLocation) {
                Global.LOCATION = location
                onLocationChanged(location)
            }
        })

        LocationHelper.getLocation(context, (20 * 1000).toLong(), object : LocationHelper.LocationResult() {
            override fun gotLocation(location: CurrentLocation) {
                Global.LOCATION = location
                onLocationChanged(location)
            }
        })
    }

    private fun onLocationChanged(location: CurrentLocation?) {
        if (location == null) LogSdk.v(TAG, "Location is null")
        else LogSdk.v(TAG, "LocationHelper: " + location.latitude.toString() + "," + location.longitude.toString())
    }

    private fun checkArSupport(activity: Activity): Boolean {
        try {
            when (ArCoreApk.getInstance().requestInstall(activity, !installRequested)!!) {
                ArCoreApk.InstallStatus.INSTALL_REQUESTED -> {
                    installRequested = true
                    return false
                }
                ArCoreApk.InstallStatus.INSTALLED -> { }
            }
        } catch (ignored: Exception) { }
        return true
    }

    private fun checkCompassSupport(activity: Activity): Boolean {
        return try {
            activity.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)
        } catch (e: Exception) {
            e.printStackTrace()
            true
        }
    }
}
