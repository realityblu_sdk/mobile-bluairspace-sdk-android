package com.bluairspace.sdk.util.sdk

import android.content.Context
import java.io.File

internal object IOUtils{

    internal lateinit var externalFilesDir: String

    fun init(context: Context) {
        if (!::externalFilesDir.isInitialized) externalFilesDir = context.getExternalFilesDir(null)!!.toString() + File.separator
    }

    fun getFileNamefromUrl(url: String): String {
        return url.substring(url.lastIndexOf('/') + 1, url.length)
    }

    fun getFileExtention(filename: String?): String {
        if (filename == null) return ""
        val dotPos = filename.lastIndexOf(".") + 1
        return filename.substring(dotPos)
    }
}
