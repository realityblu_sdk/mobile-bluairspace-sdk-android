package com.bluairspace.sdk.util.sdk.tasks

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.bluairspace.sdk.model.networkCall.CurrentLocation
import com.bluairspace.sdk.util.publicutil.PermissionUtil
import com.bluairspace.sdk.util.sdk.LogSdk
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("MissingPermission")
internal object LocationHelper {

    private val TAG = LocationHelper::class.java.simpleName

    private var timer =  Timer()

    private lateinit var locationManager: LocationManager
    private var locationResultsList: ArrayList<LocationResult?> = ArrayList()

    private var gpsEnabled = false
    private var networkEnabled = false

    private val handler = Handler(Looper.getMainLooper())

    private var locationListenerGps: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            LogSdk.d("TAG", "result from locationListenerGps")
            locationManager.removeUpdates(this)
        }

        override fun onProviderDisabled(provider: String) {}

        override fun onProviderEnabled(provider: String) {}

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    }

    private var locationListenerNetwork: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            LogSdk.d(TAG, "result from locationListenerNetwork")
            locationManager.removeUpdates(this)
        }

        override fun onProviderDisabled(provider: String) {}

        override fun onProviderEnabled(provider: String) {}

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    }

    fun removeArTasks() {
        if (locationResultsList.size != 0) {
            for (i in locationResultsList.size - 1 downTo locationResultsList.size - 2) {
                locationResultsList[i] = null
                locationResultsList.removeAt(i)
            }
        }
        handler.removeCallbacksAndMessages(null)
    }

    fun getLocation(context: Context, timeOut: Long, result: LocationResult): Boolean {
        locationResultsList.add(result)
        if (!::locationManager.isInitialized) locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!PermissionUtil.isPermissionsGranted(context)) return false

        gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (!gpsEnabled && !networkEnabled) return false
        if (gpsEnabled) locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListenerGps)
        if (networkEnabled) locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, locationListenerNetwork)
        timer.schedule(GetLastLocation(locationResultsList.size - 1), timeOut)
        return true
    }

    private fun getResult(location: Location?, resultNumber: Int) {
        val curLoc: CurrentLocation = when {
            location != null -> CurrentLocation(location.latitude, location.longitude)
            else -> CurrentLocation.EMPTY
        }
        if (locationResultsList[resultNumber] != null) handler.post { locationResultsList[resultNumber]!!.gotLocation(curLoc) }
    }

    private class GetLastLocation(val resultNumber: Int) : TimerTask() {
        override fun run() {
            LogSdk.d(TAG, "result from GetLastLocation(TimerTask)")

            if (locationResultsList.size <= resultNumber || locationResultsList[resultNumber] == null) return

            locationManager.removeUpdates(locationListenerGps)
            locationManager.removeUpdates(locationListenerNetwork)

            var networkLocation: Location? = null
            var gpsLocation: Location? = null
            if (gpsEnabled) gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (networkEnabled) networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

            if (gpsLocation != null && networkLocation != null) {
                LogSdk.v(TAG, "time offset between gps and network(gps-network) =" + (gpsLocation.time - networkLocation.time))
                if (gpsLocation.time > networkLocation.time) getResult(gpsLocation, resultNumber)
                else getResult(networkLocation, resultNumber)
                return
            }
            if (gpsLocation != null) {
                getResult(gpsLocation, resultNumber)
                return
            }
            if (networkLocation != null) {
                getResult(networkLocation, resultNumber)
                return
            }
            getResult(null, resultNumber)
        }
    }

    abstract class LocationResult {
        abstract fun gotLocation(location: CurrentLocation)
    }
}