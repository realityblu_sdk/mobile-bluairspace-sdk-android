package com.bluairspace.sdk.util.sdk

import android.content.Context
import com.bluairspace.sdk.activity.ar.ARActivity
import com.bluairspace.sdk.model.ARScreenData
import com.wikitude.common.camera.CameraSettings
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.StandardCharsets
import java.util.*

internal object ARJsonParser {

    private const val AR_NAME = "name"
    private const val AR_PATH = "path"
    private const val STARTUP_CONFIGURATION = "startupConfiguration"
    private const val CAMERA_POSITION = "camera_position"
    private const val CAMERA_POSITION_FRONT = "front"
    private const val CAMERA_POSITION_BACK = "back"
    private const val CAMERA_RESOLUTION = "camera_resolution"
    private const val CAMERA_RESOLUTION_AUTO = "auto"
    private const val CAMERA_RESOLUTION_SD = "sd_640x480"
    private const val CAMERA_2_ENABLED = "camera_2_enabled"
    private const val EXTENSION_REQUIRED = "required_extensions"

    fun getArScreensFromJsonString(jsonString: String): List<ARScreenData> {
        val list = ArrayList<ARScreenData>()
        try {
            val resultArray = JSONArray(jsonString)
            for (u in 0 until resultArray.length()) {
                val jsonObject = resultArray.getJSONObject(u)
                val path = jsonObject.getString(AR_PATH)
                val startupConfig = jsonObject.getJSONObject(STARTUP_CONFIGURATION)
                val cameraPosition = parseCameraPositionFromConfig(startupConfig)
                val cameraResolution = parseCameraResolutionFromConfig(startupConfig)
                val extensions = parseActivityExtensions(jsonObject)
                val data = ARScreenData().apply {
                    this.path = path
                    activityClass = ARActivity::class.java
                    if (cameraPosition != null) this.cameraPosition = cameraPosition
                    if (extensions != null) this.extensions = extensions
                    if (cameraResolution != null) this.cameraResolution = cameraResolution
                    cameraFocusMode = CameraSettings.CameraFocusMode.CONTINUOUS
                }
                list.add(data)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return list
    }

    private fun parseActivityExtensions(jsonObject: JSONObject): List<String>? {
        if (jsonObject.has(EXTENSION_REQUIRED)) {
            val extensions = jsonObject.getJSONArray(EXTENSION_REQUIRED)
            val parsedExtensions = ArrayList<String>()
            for (i in 0 until extensions.length()) {
                val extension = extensions.getString(i)
                parsedExtensions.add(extension)
            }
            return parsedExtensions
        }
        return null
    }

    private fun parseCameraPositionFromConfig(startupConfig: JSONObject): CameraSettings.CameraPosition? {
        if (startupConfig.has(CAMERA_POSITION)) {
            when (startupConfig.getString(CAMERA_POSITION)) {
                CAMERA_POSITION_BACK -> return CameraSettings.CameraPosition.BACK
                CAMERA_POSITION_FRONT -> return CameraSettings.CameraPosition.FRONT
            }
        }
        return null
    }

    private fun parseCameraResolutionFromConfig(startupConfig: JSONObject): CameraSettings.CameraResolution? {
        if (startupConfig.has(CAMERA_RESOLUTION)) {
            when (startupConfig.getString(CAMERA_RESOLUTION)) {
                CAMERA_RESOLUTION_AUTO -> return CameraSettings.CameraResolution.AUTO
                CAMERA_RESOLUTION_SD -> return CameraSettings.CameraResolution.SD_640x480
            }
        }
        return null
    }

    fun loadStringFromAssets(context: Context, path: String): String {
        val inputStream = context.assets.open(path)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        return String(buffer, StandardCharsets.UTF_8)
    }
}
