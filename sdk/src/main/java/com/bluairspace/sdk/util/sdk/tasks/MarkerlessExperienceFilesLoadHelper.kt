package com.bluairspace.sdk.util.sdk.tasks

import android.net.Uri
import android.text.TextUtils
import com.bluairspace.sdk.model.MarkelessARObject
import com.bluairspace.sdk.model.MarkerlessExperience
import com.bluairspace.sdk.model.callback.DataCallback
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.sdk.AnalyticsUtil
import com.bluairspace.sdk.util.sdk.LogSdk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*

internal object MarkerlessExperienceFilesLoadHelper {
    private val TAG = MarkerlessExperienceFilesLoadHelper::class.java.simpleName
    private lateinit var task: Job

    internal fun loadMarkerlessExperienceFiles(listMarkerless: List<MarkerlessExperience>, callback: DataCallback<MarkelessARObject>, coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)) {
        if (::task.isInitialized && task.isActive) return
        task = TaskHelper.loadDataAsync(callback, coroutineScope) {
            load(listMarkerless, callback)
        }
    }

    internal fun cancelLoadingTask() {
        if (::task.isInitialized && task.isActive) task.cancel()
    }

    fun isTaskActive() = ::task.isInitialized && task.isActive

    private suspend fun load(listMarkerless: List<MarkerlessExperience>, callback: DataCallback<MarkelessARObject>): List<MarkelessARObject> {
        var wasDownloadedModel = false
        val outputList = ArrayList<MarkelessARObject>()

        LogSdk.v(TAG, "count object=" + listMarkerless.size)

        AnalyticsUtil.faSendMarkerlessModelsUsed(listMarkerless.map { it.id }.toIntArray())
        withContext(Dispatchers.Main) {callback.onProgress(0, listMarkerless.size) }
        for ((index, exp) in listMarkerless.withIndex()) {
            var iconPath: String
            var filePath: String

            iconPath = if (!TextUtils.isEmpty(exp.icon) && exp.icon != "null") {
                val resultDownloading = MarkerlessDownloadHelper.downloadFile(exp.icon)
                if (!TextUtils.isEmpty(resultDownloading.second)) {
                    val file = File(resultDownloading.second)
                    Uri.decode(Uri.fromFile(file).toString())
                } else {
                    LogSdk.v(TAG, "file not found icon url=" + exp.name)
                    "file:///android_asset/ic_placeholder_white.png"
                }
            } else {
                LogSdk.v(TAG, "file not found icon url=" + exp.name)
                "file:///android_asset/ic_placeholder_white.png"
            }

            val resultDownloading = MarkerlessDownloadHelper.downloadFile(exp.fileName)
            filePath = if (!TextUtils.isEmpty(resultDownloading.second)) {
                val file = File(resultDownloading.second)
                Uri.decode(Uri.fromFile(file).toString())
            } else {
                LogSdk.v(TAG, "file not found url=" + exp.fileName)
                ""
            }
            if (resultDownloading.first) { wasDownloadedModel = true }

            outputList.add(MarkelessARObject().apply {
                id = exp.id
                this.iconPath = iconPath
                this.filePath = filePath
                obj_animations = exp.animation
                setAudio(exp.audioFile, exp.audioLoop)
                setTransform(exp.scale, exp.position, exp.rotation, exp.scaleMobile)
            })
            withContext(Dispatchers.Main) {callback.onProgress(index + 1, listMarkerless.size) }
        }
        if (wasDownloadedModel) AnalyticsUtil.faSendMarkerlessClick(Global.accountId)
        return outputList
    }
}
