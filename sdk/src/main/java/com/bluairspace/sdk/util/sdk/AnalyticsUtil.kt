package com.bluairspace.sdk.util.sdk

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import com.bluairspace.sdk.model.MarkerlessModelDuration
import com.bluairspace.sdk.network.endpoints.Endpoint
import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.publicutil.UrlHelper
import com.google.firebase.analytics.FirebaseAnalytics
import java.util.*

internal object AnalyticsUtil {

    //COMMON
    private const val A_SESSION_DURATION = "ar_session_dur"

    //MARKERBASED
    private const val A_MARKERBASED_CLICK = "ar_mb_click"
    private const val A_MARKERBASED_RECOGNISED = "ar_mb_recognized"
    private const val A_MARKERBASED_EXP_DURATION = "ar_mb_exp_duration"
    private const val A_MARKERBASED_EXP_ACTION = "ar_mb_exp_action"
    private const val A_MARKERBASED_PROOFING = "ar_mb_proofing"

    //MARKERLESS
    private const val A_MARKERLESS_CLICK = "ar_ml_click"
    private const val A_MARKERLESS_MODELS_USED = "ar_ml_models_used"
    private const val A_MARKERLESS_EXP_DURATION = "ar_ml_exp_duration"

    //PARAMS
    private const val A_PARAM_MARKER_ID = "marker_id"
    private const val A_PARAM_ACCOUNT_ID = "account_id"
    private const val A_PARAM_EXPERIENCE_ID = "experience_id"
    private const val A_PARAM_DURATION = "duration"
    private const val A_PARAM_AR_TYPE = "ar_type"
    private const val A_PARAM_DEVICE_ID = "device_id"
    private const val A_PARAM_LOCATION = "location"
    private const val A_PARAM_MODEL_ID = "model_id"

    private const val A_PARAM_EXP_ACTION_TYPE = "exp_action_type"
    private const val A_PARAM_EXP_ACTION_ID = "exp_action_id"
    private const val A_PARAM_EXP_ACTION_NAME = "exp_action_name"
    private const val A_PARAM_EXP_ACTION_VALUE = "exp_action_value"

    //USER PROPERTIES
    private const val USER_ENVIRONMENT = "user_environment"

    private var isProofingMode = false
    private lateinit var modelDurations: ArrayList<MarkerlessModelDuration>
    private lateinit var fireBaseAnalytics: FirebaseAnalytics

    @SuppressLint("MissingPermission")
    fun init(context: Context) {
        if (!AnalyticsUtil::fireBaseAnalytics.isInitialized) fireBaseAnalytics = FirebaseAnalytics.getInstance(context)
        modelDurations = ArrayList()
        setEnvironment(UrlHelper.endpoint)
    }

    fun setEnvironment(endpoint: Endpoint) {
        fireBaseAnalytics.setUserProperty(
            USER_ENVIRONMENT, endpoint.name.toLowerCase())
    }

    fun enabled(value: Boolean) {
        fireBaseAnalytics.setAnalyticsCollectionEnabled(value)
    }

    //COMMON
    fun faSendSessionDuration(arType: Int, durationSeconds: Int) {
        val bundle = Bundle().apply {
            putBoolean(
                A_MARKERBASED_PROOFING,
                isProofingMode
            )
            putInt(A_PARAM_AR_TYPE, arType)
            putInt(A_PARAM_DURATION, durationSeconds)
        }
        fireBaseAnalytics.logEvent(A_SESSION_DURATION, bundle)
    }

    //SCAN
    fun faSendMarkerbasedClick(markerId: Int, expId: Int) {
        val bundle = Bundle().apply {
            putBoolean(
                A_MARKERBASED_PROOFING,
                isProofingMode
            )
            putInt(A_PARAM_MARKER_ID, markerId)
            putInt(A_PARAM_EXPERIENCE_ID, expId)
        }
        fireBaseAnalytics.logEvent(A_MARKERBASED_CLICK, bundle)
    }

    fun faSendMarkerbasedRecognized(markerId: Int, expId: Int) {
        val bundle = Bundle().apply {
            putBoolean(
                A_MARKERBASED_PROOFING,
                isProofingMode
            )
            putInt(A_PARAM_MARKER_ID, markerId)
            putInt(A_PARAM_EXPERIENCE_ID, expId)
            putString(
                A_PARAM_DEVICE_ID,
                MainSettings.deviceId
            )
            putString(A_PARAM_LOCATION, Global.LOCATION.shortString)
        }
        fireBaseAnalytics.logEvent(A_MARKERBASED_RECOGNISED, bundle)
    }

    fun faSendMarkerbasedExpDuration(expId: Int, duration: Int) {
        val bundle = Bundle().apply {
            putBoolean(
                A_MARKERBASED_PROOFING,
                isProofingMode
            )
            putInt(A_PARAM_EXPERIENCE_ID, expId)
            putInt(A_PARAM_DURATION, duration)
            putString(
                A_PARAM_DEVICE_ID,
                MainSettings.deviceId
            )
            putString(A_PARAM_LOCATION, Global.LOCATION.shortString)
        }
        fireBaseAnalytics.logEvent(A_MARKERBASED_EXP_DURATION, bundle)
    }

    fun faSendMarkerbasedExpAction(expId: Int, type: Int, contentId: String, name: String, value: String) {
        val bundle = Bundle().apply {
            putBoolean(
                A_MARKERBASED_PROOFING,
                isProofingMode
            )
            putInt(A_PARAM_EXPERIENCE_ID, expId)
            putInt(A_PARAM_EXP_ACTION_TYPE, type)
            putString(A_PARAM_EXP_ACTION_ID, contentId)
            putString(A_PARAM_EXP_ACTION_NAME, name)
            putString(A_PARAM_EXP_ACTION_VALUE, value)
        }
        fireBaseAnalytics.logEvent(A_MARKERBASED_EXP_ACTION, bundle)
    }

    //MARKERLESS
    fun faSendMarkerlessClick(accountId: Int) {
        val bundle = Bundle()
        bundle.putInt(A_PARAM_ACCOUNT_ID, accountId)
        fireBaseAnalytics.logEvent(A_MARKERLESS_CLICK, bundle)
    }

    fun faSendMarkerlessModelsUsed(experienceIdsArray: IntArray) {
        val bundle = Bundle().apply {
            for (i in experienceIdsArray.indices) {
                putInt("experience_$i", experienceIdsArray[i])
            }
            putString(
                A_PARAM_DEVICE_ID,
                MainSettings.deviceId
            )
            putString(A_PARAM_LOCATION, Global.LOCATION.shortString)
        }
        fireBaseAnalytics.logEvent(A_MARKERLESS_MODELS_USED, bundle)
    }

    fun setProofingMode(proofingMode: Boolean) {
        isProofingMode = proofingMode
    }

    fun sendMarkerlessDuration() {
        for (duration in modelDurations) {
            val bundle = Bundle().apply {
                putInt(A_PARAM_MODEL_ID, duration.id)
                putLong(A_PARAM_DURATION, duration.pauseDuration())
                putString(
                    A_PARAM_DEVICE_ID,
                    MainSettings.deviceId
                )
                putString(A_PARAM_LOCATION, Global.LOCATION.shortString)
            }
            fireBaseAnalytics.logEvent(A_MARKERLESS_EXP_DURATION, bundle)
        }
        modelDurations.clear()
    }

    fun addModel(modelId: Int) {
        val duration = MarkerlessModelDuration(modelId, Date().time)
        modelDurations.add(duration)
    }

    fun pauseMarkerlessDuration() {
        modelDurations.forEach { duration -> duration.pauseDuration() }
    }

    fun setMarkerlessDuration() {
        modelDurations.forEach { duration -> duration.setDuration() }
    }
}
