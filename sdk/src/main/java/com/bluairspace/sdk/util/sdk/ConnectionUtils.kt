package com.bluairspace.sdk.util.sdk

import com.bluairspace.sdk.util.publicutil.Global
import com.bluairspace.sdk.util.publicutil.MainSettings
import com.bluairspace.sdk.util.sdk.tasks.InitAppHelper
import java.io.IOException
import java.net.InetAddress

internal object ConnectionUtils {

    internal fun isInternetAvailable(): Boolean {
        return try {
            InetAddress.getByName("google.com").run { toString() != "" }
        } catch (e: Exception) {
            false
        }
    }

    internal suspend fun isInitSuccess() {
        if (!isInternetAvailable()) throw IOException("Check connection to internet")
        if (!Global.INIT_SUCCESSFUL) InitAppHelper.initSdk(MainSettings.trialEmail)
    }
}
