package com.bluairspace.sdk.util.publicutil

import com.bluairspace.sdk.model.networkCall.CurrentLocation
import java.math.BigInteger
import java.security.MessageDigest

object Global {

    var DEBUG = false
    internal var INIT_SUCCESSFUL = false
    internal var APPLICATION_ID = ""
    internal var API_KEY = ""
    internal var AR_KEY = ""
    internal var accountId = 0
    internal const val PLATFORM = "android"
    internal const val VERSION_SDK_NAME = "1.0.0"
    internal const val VERSION_SDK_CODE = 2
    internal const val MAX_COUNT_MARKERLESS_MODELS = 5

    internal val SECRET_APP = arrayOf(
        "26bf632454f8eb5983885819285e1ceca3e521b28c9e28ca92beddda30d9a9a8",
        "cb2c75051ab73c3cd62de89737baab8e7931b3be2d4447cc101fa64690b0aa4f")

    internal var LOCATION = CurrentLocation.EMPTY

    internal fun clearInit() {
        AR_KEY = ""
        INIT_SUCCESSFUL = false
    }

    object ConstantsSdk {
        internal const val phonePrefix = "tel:"
        internal const val phonePatternError = "value isn't phone"
        internal const val emailPatternError = "value isn't e-mail"
        internal const val parseCalendarEventError = "Can't obtain calendar event"
        internal const val arInitError = "Unable to obtain AR initialization data. "
        internal const val compassSupportError = "This device is not equipped with an onboard magnetometer and will be unable to display Markerless AR experiences."
        internal const val userNotFoundError = "Username not found"
        internal const val getExpByIdError = "Experience doesn't exist"
        const val arCoreSupportError = "ArCore doesn't exist"
        const val permissionError = "App hasn't got permissions"
        const val videoError = "Unsupported video format"
    }
}

fun String.sha256(): String {
    val md = MessageDigest.getInstance("SHA-256")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}