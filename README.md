## Installation
You can integrate BLU SDK into your project manually by following these steps:
1.	Copy ArModule.aar app/libs/ in your application;
2.	Your applicationId must be equals with your id in RealityBlu system
3.	 Add ArModule and required dependencies in gradle dependencies section

Look at an example and required dependencies below:
```groovy
android {
    defaultConfig {
        applicationId "your_package"
        //…
    }
}

repositories {
    flatDir {
        dirs 'libs'
    }
}

dependencies {
    //…
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation (name: 'ArModule', ext: 'aar')
    implementation ('com.google.ar:core:1.1.0')

    implementation 'com.squareup.retrofit2:retrofit:2.3.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'
    implementation 'com.squareup.okhttp3:okhttp:3.9.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.8.0'

    implementation 'com.google.firebase:firebase-core:16.0.0'
    implementation 'com.yanzhenjie.zbar:camera:1.0.0'
    //…
}
```

## BLUairspace SDK
BLUairspace SDK provides very simple interface that allows you to feel all the magic of Augumented Reality. BLUairspace SDK supports Markerbased AR and Markerless AR workflows and all the functionality of the BLUairspace platform.

**Markerbased AR** gives you an opportunity to detect, track and build entertaining experiences around a marker image.

With the help of **Markerless AR** you can place the object on the surface and it will be shown as if it's in the physical location. It can be moved, resized and (you can) interacted with from all angles.

## Initialization
Before we start please visit our [BLUairspace](https://www.realityblu.com) site to obtain unique RealityBLU license key linked to bundle identifier of your application.
Using either Markerbased or Markerless AR functionality of SDK, requires an initialization procedure to make basic SDK preparations for AR experiences. To do this just add the following code inside your Application class:
```kotlin
Blu.init(context: Context, sdkKey: String, callback: TaskCallback?)
```

## Markerbased
Marker-based API has two main features:
1.	Launching scanning screen
2.	Downloading markers (optional)

#### Launching Scanning Screen
Calling this API opens a new screen that allows scanning markers and displaying experiences when the marker is detected.        
Launch the scanning screen, you need to make the following call:
```kotlin
Blu.startMarkerbased(activity: FragmentActivity, callback: TaskCallback, markerBasedSettings: MarkerBasedSettings);
```
SDK opens the camera screen and the user can point the camera at the marker. If the marker is recognized, the experience will be downloaded and shown.

#### Marker Images
BLUairspace platform allows to upload markers and associate them with your application. This is sufficient for letting your end-users download and print markers on their own. Please note that the list of downloadable markers is configured separately from the markers you use in your experiences. 
The object that represents such an entity called `MarkerbasedMarker`.

You can get the list of configured markers using BLUDataHelper:
```kotlin
BluDataHelper.getMarkerbasedMarkers(callback: DataCallback<MarkerbasedMarker>)
```

## Markerless

Markerless API allows you to upload your own models and create groups with them, that allows user find what they need to see. 

For handy work within this structure BLU SDK provides objects of two types: `MarkerlessGroup` and `MarkerlessExperience`. One `MarkerlessGroup` could contain many `MarkerlessExperience`.

BluDataHelper is object responsible for managing of all the BLUs data models.
To get the list of formed groups, you'll need to make the following method call:

```kotlin
BluDataHelper.getMarkerlessGroups(callback: DataCallback<MarkerlessGroup>)
```

After that you could obtain `MarkerlessExperience` list out of selected group simply sending `groupId` parameter into `getMarkerlessExperiences` static function: 

```kotlin
BluDataHelper.getMarkerlessExperiences(groupId: Int, callback: DataCallback<MarkerlessExperience>)
BluDataHelper.getMarkerlessExperienceById(expId: Int, callback: DataCallback<MarkerlessExperience>)
```

Markerless API helps you to download and initialize user selected experiences as well as prepare augmented reality camera screen to appear. To achieve this simply use `startMarkerless` method:

```kotlin
Blu.startMarkerless(activity: FragmentActivity, list: List<MarkerlessExperience>, callback: TaskCallback)
```
Or you can open markerless screen only with 1 experience with:
```kotlin
Blu.startMarkerlessById(activity: FragmentActivity, id: Int, callback: TaskCallback)
```

## AR controller UI customization

You can customize UI components such as images of scanning and icons of buttons by adding `BLUcustomization` folder into your project's `assets` folder.

`BLUcustomization` requires `customization.json` file inside, desribing folder structure for the image and font resources.

Basic json file should look as follows:
```javascript
{
    "markerbased": {
        "scanning-spinner": "/markerbased/scanning-spinner.png",
        "scanning-spinner-text-svg": "/markerbased/scanning-spinner.svg",
        "loading-spinner": "/markerbased/loading-spinner.png",
        "loading-spinner-frames": 48,
        "scanning-spinner-size": 60,    //(default: 50)
        "camera-switch": "/markerbased/camera-switch.png",
        "lock-screen-on": "/markerbased/lock-screen-on.png",
        "lock-screen-off": "/markerbased/lock-screen-off.png",
        "qr-button": "/markerbased/qr-button.png"
    },
    "markerless": {
        "back-to-gallery": "/markerless/back-to-gallery.png",
        "snapshot": "/markerless/cam-photo.png",
        "surface-spinner-sprite-search": "/markerless/sprite_color.png",
        "surface-spinner-sprite-ready": "/markerless/sprite_white.png"
    },
    "common": {
        "back-button": "/common/back-button.png",
        "snapshot": "/common/snapshot.png",
        "flight-off": "/common/flight-off.png",
        "flight-on": "/common/flight-on.png",
        "icons-size" : 12   //(default: 9)
    }
}
```

> You can also find an example of `BLUcustomization` folder within delivered zip archive

## Permissions handling
SDK needs camera, locations and storage permissions. All permissions should be handled by a developer, but you can use SDK’s util methods in `PermissionUtil` object:
```kotlin
isPermissionsGranted(context: Context): Boolean
askPermission(activity: Activity)
isPermissionsGrantedAndAsk(activity: Activity, requestCode: Int, grantResults: IntArray): Boolean
setSnackBarTextColor(color: Int)
setSnackBarBackgroundColor(color: Int)
```
For example:
```kotlin
if (!PermissionUtil.isPermissionsGranted(getContext())) {
    PermissionUtil.askPermission(getActivity())
}


override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    if (PermissionUtil.isPermissionsGrantedAndAsk(this, requestCode, grantResults)) {
        //Blu.startMarkerbased()
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
}

```
